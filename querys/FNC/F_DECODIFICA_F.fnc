CREATE OR REPLACE FUNCTION FEDLOGDB.F_DECODIFICA_F(strP_MRC IN VARCHAR2, strP_RESP IN VARCHAR2) RETURN VARCHAR2 IS
  /*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    AUTOR    : 1T (T) ANDERS
    DATA    : 13/11/2017
    EXEMPLO : F_DECODIFICA_F('AECR', 'M2.0' ) -- SEM MC 
    RETORNO : RESPOSTA DECODIFICADA EM TEXTO CLARO
              no exemplo: o retorno eh: F_DECODIFICA_F('AECR', 'M2.0' ) // - TO -2.0
    VER: 1.0.0 (22/11/2017)
    VER: 2.0.0 (16/01/2018)
  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
  --- Variaveis
    strRESP1    VARCHAR2(500);
    strRESP2    VARCHAR2(500);
    strRETORNO    VARCHAR2(5000);
    strTO_OR_BY    VARCHAR2(2);  -- UTILIZADO PARA SABERMOS SE A RESPOSTA TEM "TO OU BY"
    intX         NUMBER;
BEGIN
    -- OBTEMOS O "PRT_SKLTN_CD" 
    BEGIN
        /*
        SELECT PRT_SKLTN_CD
            INTO        strTO_OR_BY
          FROM        TIPO_CARACTERISTICA_ITEM     -- Corresponde ao campo DUMP_GLM.MRD0107.PRT_SKLTN_CD
         WHERE        CODIGO = strP_MRC;
         */
         SELECT PRT_SKLTN_CD_0368
            INTO        strTO_OR_BY
          FROM        BR_MRD0107 --TIPO_CARACTERISTICA_ITEM     -- Corresponde ao campo DUMP_GLM.MRD0107.PRT_SKLTN_CD
         WHERE        MRC_3445 = strP_MRC;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN strTO_OR_BY := '';
        END;

        -- RECUPERA A POSICAO DA '/' NA STRING "strP_RESP"
        intX := F_TAMANHO(1,strP_RESP,'/');
        IF (SUBSTR(strP_RESP,1,1) = 'M') THEN
            strRESP1:= '-' || SUBSTR( strP_RESP, 2, intX - 2);
        ELSE
            strRESP1:= SUBSTR( strP_RESP, 2, intX - 2);
        END IF;
        IF (SUBSTR( strP_RESP, intX + 1, 1) = 'M') THEN
            strRESP2:= '-' || SUBSTR( strP_RESP, intX + 2, LENGTH(strP_RESP) - intX - 1);
        ELSE
            strRESP2 := SUBSTR( strP_RESP, intX + 2, LENGTH(strP_RESP) - intX - 1);
        END IF;
        -- VERIFICA SE O MRC POSSUI "TO" OU "BY" NA RESPOSTA DECODIFICADA
        IF strTO_OR_BY = 'T' THEN
            strTO_OR_BY := 'TO';
        ELSE
            strTO_OR_BY := 'BY';
        END IF;
        strRETORNO := strRESP1 || ' ' || strTO_OR_BY || ' ' || strRESP2;
    RETURN strRETORNO;
EXCEPTION
    WHEN NO_DATA_FOUND THEN RETURN 'UNABLE TO DECODE ' ||  strP_RESP;
END;
/
