CREATE OR REPLACE FUNCTION F_VRF_NUMERO(str_in IN VARCHAR2) RETURN NUMBER IS
  /*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	AUTOR	: 1T (T) ANDERS
	DATA	: 13/11/2017
    EXEMPLO : F_VRF_NUMERO( '2' )
    RETORNO : Esta funcao checa se  uma string e composta apenas por caracteres numericos. Para isso,
             ela tenta converter a string para um numero, se houver algum erro, sera gerada uma excecao
             e a funcao retornara false, indicando que a string nao e numerica..
    USO     : usado na Catalogacao para auxiliar na vrf de caracteristicas.        
  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
  --- Variaveis
  num_val NUMBER;

BEGIN

  IF str_in = '' THEN
    -- ATENCAO: se o valor passado para TO_NUMBER for uma string vazia ('') o valor retornado sera 0
     num_val := 0;
  ELSE
    -- converte a string para um numero
    num_val := TO_NUMBER(str_in);    
  END IF;

  RETURN num_val;

EXCEPTION

  WHEN OTHERS THEN RETURN 0;

END;
/