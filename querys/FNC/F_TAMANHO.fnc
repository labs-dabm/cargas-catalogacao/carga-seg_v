CREATE OR REPLACE FUNCTION FEDLOGDB.F_TAMANHO( intP_POS IN NUMBER, strFRASE IN VARCHAR2, chCARACTER IN CHAR ) RETURN NUMBER IS
/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	AUTOR	: 1T (T) ANDERS
	DATA	: 13/11/2017
  EXEMPLO : F_TAMANHO( 4, '2BSDSTA000$$DRCE000#', '$' )
  RETORNO : Retorna o nro de caracteres entre a posicao passada (inicial de leitura) "intP_POS"
             e a primeira ocorrencia do caracter a ser localizado "chCARACTER".
             se nao for encotrado a funcao retorna 0.
  USO     : usado na Catalogacao para auxiliar na vrf de caracteristicas.
  VER: 1.0.0 (22/11/2017)  
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/

	intFIM NUMBER; --Armazena posicao e vai sendo add em 1 a cada analise
BEGIN

	intFIM := intP_POS;

  WHILE ( (SUBSTR(strFRASE,intFIM,1) <> chCARACTER) AND (intFIM < LENGTH(strFRASE)) )
  	LOOP
			intFIM := intFIM + 1;
    END LOOP;

  IF (SUBSTR( strFRASE, intFIM,1) <> chCARACTER) THEN
  	RETURN 0;
  ELSE
  	RETURN (intFIM - intP_POS + 1);
  END IF;

END;
/