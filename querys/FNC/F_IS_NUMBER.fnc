CREATE OR REPLACE FUNCTION F_IS_NUMBER(str_in IN VARCHAR2)
  RETURN BOOLEAN
IS
 /*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	AUTOR	: 1T (T) ANDERS
	DATA	: 13/11/2017
  EXEMPLO : F_IS_NUMBER('2') 
  RETORNO : Esta funcao checa se  uma string e composta apenas por caracteres numericos. Para isso,
              ela tenta converter a string para um numero, se houver algum erro, sera gerada uma excecao
              e a funcao retornara false, indicando que a string nao e numerica.
  VER: 1.0.0 (22/11/2017)
  XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
  --- Variaveis
  num_val NUMBER;

BEGIN

  num_val := TO_NUMBER(str_in);

  RETURN TRUE;

EXCEPTION

  WHEN OTHERS THEN RETURN FALSE;

END f_is_number;
/
