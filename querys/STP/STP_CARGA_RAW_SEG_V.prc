CREATE OR REPLACE PROCEDURE STP_CARGA_RAW_SEG_V
 ( p_CLASSE IN VARCHAR2 -- Representa O FSC DO NIIN - CLASSE DO PI. ex: '4720'
   ,p_NIIN IN VARCHAR2 -- Equivalente ao codigo PI. Nomenclatura utilizada no CECAFA/SOC. ex: '000010079'
   ,p_INC IN VARCHAR2 -- Equivalente ao Item Name Code, atrelado ao NIIN ex: 'D06338#' c/(MC/#)
   ,p_MRC IN VARCHAR2 -- Representa o Master Requeriment Code - Código de 4 caracteres. ex: 'AAJD'
   ,p_MC IN VARCHAR2 -- Mode Code - Código de 1 caracter, indica o tipo e o formato das respostas. ex: 'L'
   ,p_RESP_COD IN VARCHAR2 -- RESPOSTA CODIFICADA SEM (MRC , MODECODE E #). ex: '2B'
   ,p_SAIC IN VARCHAR2 -- Tamanho do SAC, determina quantos caracteres possui o SAC. ex: '1'
   ,p_SAC IN VARCHAR2 -- Modificador do MRC, cód permite um grau de especificidade na resposta do MRC. ex: 'B'
   ,p_RC_RAW IN VARCHAR2 -- RC Reply Code Raw, representa a resposta codificado bruta sem #. ex: 'AAJD1BA2B'
)
 IS
/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
AUTOR	: 1T (T) ANDERS
DATA	: 13/11/2017
EXEMPLO : STP_CARGA_RAW_SEG_V('4720','000010079', 'D06338#', 'AAJD', 'L', '2B', '1', 'B', 'AAJD1BA2B')
VER: 1.0.0 (22/11/2017)
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
--- Variaveis
-- PL/SQL Specification
vFIIG               VARCHAR2(100);  -- GUARDA O VALOR FIIG 
vMRC_DECODE         VARCHAR2(4000); -- GUARDA O VALOR DECODIFICADO DO MRC
vCLEAR_REPLY_DECODE VARCHAR2(4000); -- GUARDA O VAOR DECODIFICADO DA RESPOSTA PASSDA
vRESP_CODIFICADA    VARCHAR2(4000); -- GUARDA A RESP COM (MC + #)
vINC                VARCHAR2(10); -- GUARDA O VALOR DO INC SEM (MC/#)
vNUM_POS_TRALHA     NUMBER; --GUARDA A POSICAO DA TRALHA NA STRING PASSADA

-- PL/SQL Block
BEGIN
    --retorna o numero inteiro da posicao onde esta localizado o # na string passada
    vNUM_POS_TRALHA := INSTR(p_INC, '#');
    -- retorna o valor do INC sem (MC e #)
    vINC := SUBSTR(p_INC, 2, vNUM_POS_TRALHA-2);
    -- vrf passando o INC se tem FIIG, se tiver retorna a FIIG
    vFIIG := F_DECODIFICA_FIIG(vINC);
    -- add o MC e o # na resposta para ficar no padrao
    vRESP_CODIFICADA := p_MC || p_RESP_COD || '#';
    -- retorna o MRC_REQUIREMENT_STATEMENT, passando o MRC
    vMRC_DECODE := F_DECODIFICA_MRC(p_MRC);

    -- retorna a resposta decodificada
    vCLEAR_REPLY_DECODE := F_CLEAR_REPLY(p_INC, vFIIG, p_MRC, vRESP_CODIFICADA);

    INSERT 
      INTO NATOCHAR(CLASSE, NIIN, INC, MRC, MC, MRC_REQUIREMENT_STATEMENT, RESP_COD, SAIC, SAC, RC_RAW, CLEAR_REPLY) 
    VALUES (p_CLASSE, p_NIIN, p_INC, p_MRC, p_MC, vMRC_DECODE, p_RESP_COD, p_SAIC, p_SAC, p_RC_RAW, vCLEAR_REPLY_DECODE);

EXCEPTION
    WHEN OTHERS THEN 
        RAISE_APPLICATION_ERROR(-20999,'ATENÇÃO! NÃO FOI POSSÍVEL CONCLUIR. NIIM: ' 
                                || p_NIIN || 'FSC: ' || p_CLASSE
                                || 'INC: ' || p_INC 
                                ' ERRO: ' || SQLERRM); 
                                  
END;
/