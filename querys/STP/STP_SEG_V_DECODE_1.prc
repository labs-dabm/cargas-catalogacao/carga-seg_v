CREATE OR REPLACE PROCEDURE STP_SEG_V_DECODE1
( 
  p_LinhaInicial IN NUMBER, -- Representa a linha inicial para fazer a consulta paginando
  p_LinhaFinal IN NUMBER, -- Representa a linha Final para a consulta
  p_Limite IN NUMBER -- Representa o limite para o commit
) 
IS
/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
AUTOR	: 1T (T) ANDERS
DATA	: 12/01/2018
EXEMPLO : STP_SEG_V_DECODE
VER: 1.0.0 (12/01/2018)
XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
--- Variaveis
-- PL/SQL Specification
vFIIG               VARCHAR2(100);  -- GUARDA O VALOR FIIG 
vMRC_DECODE         VARCHAR2(4000); -- GUARDA O VALOR DECODIFICADO DO MRC
vCLEAR_REPLY_DECODE VARCHAR2(4000); -- GUARDA O VAOR DECODIFICADO DA RESPOSTA PASSDA
vRESP_CODIFICADA    VARCHAR2(4000); -- GUARDA A RESP COM (MC + #)
vINC                VARCHAR2(400);

CURSOR c_segv_cursor IS    
    SELECT s.NRLINHA, s.NIIN, s.CLASSE, s.INC, s.MRC, s.MC, s.RESP_COD, s.SAIC, s.SAC, s.RC_RAW
    FROM (SELECT ROWNUM AS NRLINHA, s.*
            FROM (SELECT *
                    FROM (
                          SELECT nc.*
                            FROM NATOCHARCODE nc order by niin
                         )
                   WHERE ROWNUM <= p_LinhaFinal
                     ) s) s
   WHERE (p_LinhaInicial <= NRLINHA)
ORDER BY NRLINHA;

TYPE t_teste IS TABLE OF c_segv_cursor%rowtype;   
v_segv_record t_teste;

-- PL/SQL Block    
BEGIN
   OPEN c_segv_cursor;
     LOOP
       FETCH c_segv_cursor bulk collect INTO v_segv_record LIMIT p_Limite;
        FOR i IN 1 .. v_segv_record.count
            LOOP
                -- vrf passando o INC se tem FIIG, se tiver retorna a FIIG
                vFIIG := F_DECODIFICA_FIIG (SUBSTR (v_segv_record(i).INC, 2));
                -- add o MC e o # na resposta para ficar no padrao
                vRESP_CODIFICADA := v_segv_record(i).MC || v_segv_record(i).RESP_COD || '#';
                -- retorna o MRC_REQUIREMENT_STATEMENT, passando o MRC
                vMRC_DECODE := F_DECODIFICA_MRC(v_segv_record(i).MRC);

                IF v_segv_record(i).INC IS NOT NULL then
                  vINC := v_segv_record(i).INC || '#';
                END IF;

                -- retorna a resposta decodificada
                vCLEAR_REPLY_DECODE := F_CLEAR_REPLY(vINC, vFIIG, v_segv_record(i).MRC, vRESP_CODIFICADA);

                INSERT INTO NATOCHAR (CLASSE, NIIN, INC, MRC, MC, MRC_REQUIREMENT_STATEMENT, RESP_COD, SAIC, SAC, RC_RAW, 
                                    CLEAR_REPLY)
                VALUES (v_segv_record(i).CLASSE, v_segv_record(i).NIIN, vINC,  v_segv_record(i).MRC, 
                        v_segv_record(i).MC, vMRC_DECODE, v_segv_record(i).RESP_COD, 
                        v_segv_record(i).SAIC, v_segv_record(i).SAC, v_segv_record(i).RC_RAW, vCLEAR_REPLY_DECODE);
                
                commit; -- commit a cada 2.000 registros

            END LOOP;
       EXIT WHEN c_segv_cursor%NOTFOUND;
     END LOOP;
   CLOSE c_segv_cursor;  

EXCEPTION
    WHEN OTHERS THEN 
        RAISE_APPLICATION_ERROR(-20999,'ERRO: ' || SQLCODE || SQLERRM); 
                                  
END;
/