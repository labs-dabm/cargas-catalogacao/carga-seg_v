DROP TABLE FEDLOGDB.NATOCHARCODE CASCADE CONSTRAINTS;

CREATE TABLE FEDLOGDB.NATOCHARCODE
(
  CLASSE    VARCHAR2(4 BYTE),
  NIIN      NUMBER                              NOT NULL,
  INC       VARCHAR2(50 BYTE),
  MRC       VARCHAR2(4 BYTE),
  MC        VARCHAR2(1 BYTE),
  RESP_COD  VARCHAR2(4000 BYTE),
  SAIC      VARCHAR2(1 BYTE),
  SAC       VARCHAR2(100 BYTE),
  RC_RAW    VARCHAR2(4000 BYTE)
)
TABLESPACE CATALOG_TAB01
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON TABLE FEDLOGDB.NATOCHARCODE IS 'CATALOGAÇÃO - TABELA RESPONSÁVEL PARA GUARDAR OS DADOS CARACTERÍSTICOS DO ITEM, NO PROCESSO DE IMPORTAÇÃO DO SEGMENTO V - RAW DATA - Autor: 1T(T) Anders';

COMMENT ON COLUMN FEDLOGDB.NATOCHARCODE.CLASSE IS 'FSC DO NIIN - CLASSE DO PI';

COMMENT ON COLUMN FEDLOGDB.NATOCHARCODE.NIIN IS 'NIIN - Número de identificação do item. Equivalente ao código PI. Nomenclatura utilizada no CECAFA/SOC';

COMMENT ON COLUMN FEDLOGDB.NATOCHARCODE.INC IS 'INC - Item Name Code - contem os nomes aprovados cobertos pelo FIIG (MC+MRC+#)';

COMMENT ON COLUMN FEDLOGDB.NATOCHARCODE.MRC IS 'MRC - Master Requeriment Code - Código de 4 caracteres que pode ser interpretado com sendo uma pergunta';

COMMENT ON COLUMN FEDLOGDB.NATOCHARCODE.MC IS 'MC - Mode Code - Código de 1 caracter, indica o tipo e o formato das respostas: (texto claro, números em formado decimal, tabelada, combinadas e figuras)';

COMMENT ON COLUMN FEDLOGDB.NATOCHARCODE.RESP_COD IS 'Resposta codificada para o tipo de característica de item (MRC)';

COMMENT ON COLUMN FEDLOGDB.NATOCHARCODE.SAIC IS 'Tamanho do SAC, determina quantos caracteres possui o SAC - pertence ao grupo I/SAC';

COMMENT ON COLUMN FEDLOGDB.NATOCHARCODE.SAC IS 'Modificador do MRC, código alfanumérico que permite um grau de especificidade na resposta do MRC';

COMMENT ON COLUMN FEDLOGDB.NATOCHARCODE.RC_RAW IS 'RC Reply Code Raw, representa a resposta codificado bruta com o # no final';



CREATE INDEX FEDLOGDB.IDX_NATOCHARCODE_NIIN_01 ON FEDLOGDB.NATOCHARCODE
(NIIN)
NOLOGGING
TABLESPACE CATALOG_TAB01
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;
