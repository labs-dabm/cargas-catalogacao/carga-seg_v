package mb.dabm.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import mb.dabm.database.IPool;

public class TesteDAO
{
    private IPool pool;
    private Connection conn;

    public TesteDAO(IPool pool)
    {
	this.pool = pool;
	this.conn = pool.getConnection();
    }

    public String getDateBD() throws SQLException
    {
	PreparedStatement ps = null;
	ResultSet rs = null;

	String sqlSelect = "SELECT sysdate FROM dual";

	try {
	    ps = this.conn.prepareStatement(sqlSelect);
	    rs = ps.executeQuery();

	    if (rs.next()) {

		System.out.println(rs.getDate("sysdate"));
		return rs.getDate("sysdate").toString();
	    }

	    rs.close();
	    ps.close();

	} finally {
	    pool.liberarConnection(this.conn);
	}

	return null;
    }
}
