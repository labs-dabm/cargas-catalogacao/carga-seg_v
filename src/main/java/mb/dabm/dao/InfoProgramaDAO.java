package mb.dabm.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mb.dabm.database.IPool;
import mb.dabm.model.InformacoesPrograma;

public class InfoProgramaDAO
{
    private IPool pool;
    private Connection conn;

    private static final Logger LOGGER = LogManager.getLogger(InfoProgramaDAO.class.getName());

    public InfoProgramaDAO(IPool pool)
    {
	this.pool = pool;
	this.conn = pool.getConnection();
    }

    public InformacoesPrograma buscar()
    {
	String sql = "SELECT to_char(DATA_DO_RAW, 'MON/YYYY') AS DATA_MES, DATA_DO_RAW, MANUTENCAO, QTD_REGISTROS, QTD_LINHAS, QTD_DADOS_TB "
		+ "FROM SEGV_DATA_ATUALIZACAO WHERE PROGRAMA=?";
	InformacoesPrograma retorno = new InformacoesPrograma();
	try {
	    PreparedStatement stmt = this.conn.prepareStatement(sql);
	    stmt.setString(1, "SEGMENTO_V");
	    ResultSet resultado = stmt.executeQuery();
	    if (resultado.next()) {
		retorno.setDataMes(resultado.getString("DATA_MES"));
		retorno.setDataRawData(resultado.getDate("DATA_DO_RAW"));
		retorno.setManutencao(resultado.getString("MANUTENCAO"));
		retorno.setQtdRegistros(resultado.getLong("QTD_REGISTROS"));
		retorno.setQtdLinhas(resultado.getLong("QTD_LINHAS"));
		retorno.setQtdRowsTabela(resultado.getLong("QTD_DADOS_TB"));

	    }
	} catch (SQLException ex) {
	    if (LOGGER.isErrorEnabled()) {
		LOGGER.error("[SQLException] " + ex.getMessage());
	    }
	}
	return retorno;
    }

    public int buscarTotalDadosInseridosTabela()
    {
	String sql = "SELECT QTD_DADOS_TB FROM SEGV_DATA_ATUALIZACAO WHERE PROGRAMA=?";

	try {
	    PreparedStatement stmt = this.conn.prepareStatement(sql);
	    stmt.setString(1, "SEGMENTO_V");
	    ResultSet resultado = stmt.executeQuery();

	    if (resultado.next()) {
		return resultado.getInt("QTD_DADOS_TB");
	    }

	} catch (SQLException ex) {
	    if (LOGGER.isErrorEnabled()) {
		LOGGER.error("[SQLException] " + ex.getMessage());
	    }

	}

	return 0;
    }

    public boolean updateManut(String info)
    {
	String sql = "UPDATE SEGV_DATA_ATUALIZACAO SET MANUTENCAO=? WHERE PROGRAMA=?";
	// PreparedStatement pstmt = null;

	try {
	    PreparedStatement pstmt = this.conn.prepareStatement(sql);
	    pstmt.setString(1, info);
	    pstmt.setString(2, "SEGMENTO_V");
	    pstmt.execute();
	    return true;
	} catch (SQLException e) {
	    if (LOGGER.isErrorEnabled()) {
		LOGGER.error("[SQLException] " + e.getMessage());
	    }

	    return false;
	} finally {
	    pool.liberarConnection(this.conn);
	}
    }

    public boolean updateTotalRegistrosNaTabela(int total)
    {
	String sql = "UPDATE SEGV_DATA_ATUALIZACAO SET QTD_DADOS_TB=? WHERE PROGRAMA=?";
	// PreparedStatement pstmt = null;

	try {
	    PreparedStatement pstmt = this.conn.prepareStatement(sql);
	    pstmt.setInt(1, total);
	    pstmt.setString(2, "SEGMENTO_V");
	    pstmt.execute();
	    return true;
	} catch (SQLException e) {
	    if (LOGGER.isErrorEnabled()) {
		LOGGER.error("[SQLException] " + e.getMessage());
	    }

	    return false;
	} finally {
	    pool.liberarConnection(this.conn);
	}
    }

    public boolean updateInfo(InformacoesPrograma info)
    {
	String sql = "UPDATE SEGV_DATA_ATUALIZACAO SET DATA_DO_RAW=?, MANUTENCAO=?, QTD_REGISTROS=?, QTD_LINHAS=? WHERE PROGRAMA=?";
	// PreparedStatement pstmt = null;

	try {
	    PreparedStatement pstmt = this.conn.prepareStatement(sql);
	    pstmt.setDate(1, info.getDataRawData());
	    pstmt.setString(2, info.getManutencao());
	    pstmt.setLong(3, info.getQtdRegistros());
	    pstmt.setLong(4, info.getQtdLinhas());
	    pstmt.setString(5, "SEGMENTO_V");
	    pstmt.execute();
	    return true;
	} catch (SQLException e) {
	    if (LOGGER.isErrorEnabled()) {
		LOGGER.error("[SQLException] " + e.getMessage());
	    }

	    return false;
	} finally {
	    pool.liberarConnection(this.conn);
	}
    }

}
