package mb.dabm.dao;

import java.sql.BatchUpdateException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import mb.dabm.database.IPool;
import mb.dabm.model.CharacteristicsSegV;
import mb.dabm.model.SegV;
import mb.dabm.model.SegmentV;
import mb.dabm.model.SegmentoV;

public class SegmentVDAO
{

    private IPool pool;
    private Connection conn;

    private static final Logger LOGGER = LogManager.getLogger(SegmentVDAO.class.getName());

    public SegmentVDAO(IPool pool)
    {
	this.pool = pool;
	this.conn = pool.getConnection();
    }

    /**
     * Faz Insert no BD por lista de SegV e suas caracteristicas, por meio de
     * INSERT.
     * 
     * Metodo estah depreciado, pois foi criado uma procedure para abstrair a
     * complexidade da entrada de dados para insercao das caracteristicas do seg V.
     * 
     * @param -
     *            lista de Objeto <SegmentV>
     * @deprecated
     * @return - tota de querys inseridas no BD com sucesso, final -2
     * 
     * @throws SQLException
     */
    public Long insertBatch(List<SegmentV> lista) throws SQLException
    {
	int[] num = null;
	// Set auto-commit to false
	this.conn.setAutoCommit(false);
	String insertSql = "INSERT INTO NATOCHAR (CLASSE, NIIN, INC, MRC, MC, MRC_REQUIREMENT_STATEMENT, RESP_COD, SAIC, SAC, RC_RAW, CLEAR_REPLY) "
		+ "VALUES (?,?,?,?,?,F_DECODIFICA_MRC(?),?,?,?,?,F_CLEAR_REPLY(? , F_DECODIFICA_FIIG(?), ?, ?))";
	PreparedStatement pstmt = conn.prepareStatement(insertSql);

	try {

	    for (SegmentV segV : lista) {

		List<CharacteristicsSegV> listaCarSegV = (List<CharacteristicsSegV>) segV.getListaCharacteristicsSegV();
		listaCarSegV.forEach(carSegV -> {
		    try {

			String v = carSegV.getMc() + carSegV.getRc() + "#";

			pstmt.setString(1, segV.getNSC());
			pstmt.setInt(2, Integer.valueOf(segV.getNIIN()));
			pstmt.setString(3, segV.getInc());
			pstmt.setString(4, carSegV.getMrc());
			pstmt.setString(5, carSegV.getMc());
			pstmt.setString(6, carSegV.getMrc());
			pstmt.setString(7, carSegV.getRc());
			pstmt.setString(8, String.valueOf(carSegV.getSaic()));
			pstmt.setString(9, carSegV.getSac());
			pstmt.setString(10, carSegV.getRaw());
			pstmt.setString(11, segV.getInc());
			pstmt.setString(12, segV.getInc_nro());
			pstmt.setString(13, carSegV.getMrc());
			pstmt.setString(14, v);

			pstmt.addBatch();

		    } catch (SQLException e) {
			System.err.println("erro:" + e.getMessage());
		    }

		});

	    }

	    // Create an int[] to hold returned values
	    num = pstmt.executeBatch();

	    // Explicitly commit statements to apply changes
	    this.conn.commit();

	    // limpa o objeto stmt
	    pstmt.clearBatch();

	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    pstmt.close();
	    pool.liberarConnection(this.conn);
	}

	Long cont = 0L;

	for (int i = 0; i < num.length; i++) {
	    if (num[i] == -2) {
		// System.out.println("A query " + i + " n�o afetou nenhuma linha");
		cont++;
	    }
	}

	return cont;

    }

    /**
     * Faz Insert no BD por lista de SegV e suas caracteristicas, por meio de uma
     * Procedure
     * 
     * @deprecated - pois demora muito para atualizar todos os registros
     * @param -
     *            lista de Objeto <SegmentV>
     * @return - tota de querys inseridas no BD com sucesso.
     * @throws SQLException
     */
    public Long insertBatchProcedure(List<SegmentV> lista) throws SQLException, BatchUpdateException
    {
	int[] num = null;
	Long cont = 0L;
	// Set auto-commit to false
	this.conn.setAutoCommit(true);
	String insertSTP = "{call STP_CARGA_RAW_SEG_V(?,?,?,?,?,?,?,?,?)}";
	CallableStatement pstmt = conn.prepareCall(insertSTP);

	try {

	    for (SegmentV segV : lista) {

		List<CharacteristicsSegV> listaCarSegV = (List<CharacteristicsSegV>) segV.getListaCharacteristicsSegV();

		for (CharacteristicsSegV carSegV : listaCarSegV) {
		    pstmt.setString(1, segV.getNSC());
		    pstmt.setInt(2, Integer.valueOf(segV.getNIIN()));
		    pstmt.setString(3, segV.getInc());
		    pstmt.setString(4, carSegV.getMrc());
		    pstmt.setString(5, carSegV.getMc());
		    pstmt.setString(6, carSegV.getRc());
		    pstmt.setString(7, String.valueOf(carSegV.getSaic()));
		    pstmt.setString(8, carSegV.getSac());
		    pstmt.setString(9, carSegV.getRaw());

		    pstmt.addBatch();
		}

	    }

	    // Create an int[] to hold returned values
	    num = pstmt.executeBatch();

	    // System.out.println(num.length + "- " + num.toString());

	    // Explicitly commit statements to apply changes
	    this.conn.commit();

	    // limpa o objeto stmt
	    pstmt.clearBatch();

	} catch (BatchUpdateException e) {

	    // System.err.println("SQLException: " + e.getMessage());
	    // System.err.println("SQLState: " + e.getSQLState());
	    // System.err.println("Message: " + e.getMessage());
	    // System.err.println("Vendor: " + e.getErrorCode());
	    // System.err.print("Update counts: ");
	    // int[] updateCounts = e.getUpdateCounts();
	    // for (int i = 0; i < updateCounts.length; i++) {
	    // System.err.print(updateCounts[i] + " ");
	    // }

	    int[] updateCount = e.getUpdateCounts();

	    int count = 1;
	    for (int i : updateCount) {
		if (i == Statement.EXECUTE_FAILED) {
		    System.out.println("Error on request " + count + ": Execute failed");
		} else {
		    System.out.println("Request " + count + ": OK");
		}
		count++;

	    }

	    if (LOGGER.isErrorEnabled()) {
		LOGGER.error("[BatchUpdateException] " + e.getMessage());
	    }

	    num = e.getUpdateCounts();
	    throw e;
	} catch (SQLException e) {
	    // System.err.println("[2� catch SQLException] Erro: " + e.getMessage());
	    if (LOGGER.isErrorEnabled()) {
		LOGGER.error("[SQLException] " + e.getMessage());
	    }
	    throw e;
	} finally {
	    if (pstmt != null)
		pstmt.close();

	    pool.liberarConnection(this.conn);

	    for (int i = 0; i < num.length; i++) {
		if (num[i] >= 0) {
		    cont++;
		}
	    }
	}

	return cont;

    }

    /**
     * Faz Insert no BD por lista de SegV e suas caracteristicas, por meio de
     * INSERT.
     * 
     * Metodo estah depreciado, pois foi criado uma procedure para abstrair a
     * complexidade da entrada de dados para insercao das caracteristicas do seg V.
     * 
     * @param -
     *            list de Objeto <SegmentV>
     * @return - tota de querys inseridas no BD com sucesso, final -2
     * 
     * @throws SQLException
     */
    public Long insertListaSegVBatch(List<SegmentV> lista) throws SQLException
    {
	int[] num = null;
	// Set auto-commit to false
	this.conn.setAutoCommit(true);
	String insertSql = "INSERT INTO NATOCHARCODE (ID, CLASSE, NIIN, INC, MRC, MC, RESP_COD, SAIC, SAC, RC_RAW) "
		+ "VALUES (SEQNATOCHARCODE.NEXTVAL,?,?,?,?,?,?,?,?,?)";
	PreparedStatement pstmt = conn.prepareStatement(insertSql);

	try {

	    for (SegmentV segV : lista) {

		List<CharacteristicsSegV> listaCarSegV = (List<CharacteristicsSegV>) segV.getListaCharacteristicsSegV();

		for (CharacteristicsSegV carSegV : listaCarSegV) {

		    pstmt.setString(1, segV.getNSC());
		    pstmt.setInt(2, Integer.valueOf(segV.getNIIN()));
		    pstmt.setString(3, segV.getInc());
		    pstmt.setString(4, carSegV.getMrc());
		    pstmt.setString(5, carSegV.getMc());
		    pstmt.setString(6, carSegV.getRc());
		    pstmt.setString(7, (carSegV.getSaic() != 0) ? String.valueOf(carSegV.getSaic()) : null);
		    pstmt.setString(8, carSegV.getSac());
		    pstmt.setString(9, carSegV.getRaw());

		    pstmt.addBatch();
		}

	    }

	    // Create an int[] to hold returned values
	    num = pstmt.executeBatch();

	    // Explicitly commit statements to apply changes
	    this.conn.commit();

	    // limpa o objeto stmt
	    pstmt.clearBatch();

	} catch (BatchUpdateException e) {

	    //System.err.println("SQLException: " + e.getMessage());
	    //System.err.println("SQLState: " + e.getSQLState());
	    //System.err.println("Message: " + e.getMessage());
	    //System.err.println("Vendor: " + e.getErrorCode());
	    // System.err.print("Update counts: ");
	    // int[] updateCounts = e.getUpdateCounts();
	    // for (int i = 0; i < updateCounts.length; i++) {
	    // System.err.print(updateCounts[i] + " ");
	    // }

	    /*
	     * int[] updateCount = e.getUpdateCounts();
	     * 
	     * int count = 1; for (int i : updateCount) { if (i == Statement.EXECUTE_FAILED)
	     * { System.out.println("Error on request " + count + ": Execute failed"); }
	     * else { System.out.println("Request " + count + ": OK"); } count++;
	     * 
	     * }
	     * 
	     */

	    if (LOGGER.isErrorEnabled()) {
		LOGGER.error("[BatchUpdateException] -> SQLException: " + e.getMessage());
	    }

	    //System.out.println(lista.toString());

	    num = e.getUpdateCounts();

	    e.printStackTrace();

	    // throw e;
	} catch (SQLException e) {
	    if (LOGGER.isErrorEnabled()) {
		LOGGER.error("[SQLException] " + e.getMessage());
	    }
	} finally {
	    pstmt.close();
	    pool.liberarConnection(this.conn);
	}

	Long cont = 0L;

	for (int i = 0; i < num.length; i++) {
	    if (num[i] == -2) {
		// System.out.println("A query " + i + " n�o afetou nenhuma linha");
		cont++;
	    }
	}

	return cont;

    }

    /**
     * Insere um SegV + a lista de caracteristicas do segmento
     * 
     * @param SegmentV
     * @return
     * @throws SQLException
     * 
     * @example: NIIN: 000010079 NSC: 4720 INC_NRO: 20311 INC: D20311# INC_RAW:
     *           NAMED20311# ListaReplayCode: [AAGRLA1, AAJD1BA2B, AAJD1CA1A, ...]
     *           ListaCaracteristica: [ ----CharacteristicsSegV----- MRC: AAGR
     *           ModeCode: L Saic: 0 Sac: null RespostaCode: A1 Raw: AAGRLA1#,
     *           ----CharacteristicsSegV----- ... ]
     **/
    public Long insertOneSegV(SegmentV segV) throws SQLException// , BatchUpdateException
    {

	int[] num = null;
	Long cont = 0L;

	this.conn.setAutoCommit(true);
	String insertSql = "INSERT INTO NATOCHARCODE (ID, CLASSE, NIIN, INC, MRC, MC, RESP_COD, SAIC, SAC, RC_RAW) "
		+ "VALUES (SEQNATOCHARCODE.NEXTVAL,?,?,?,?,?,?,?,?,?)";
	PreparedStatement pstmt = conn.prepareStatement(insertSql);

	try {

	    List<CharacteristicsSegV> listaCarSegV = (List<CharacteristicsSegV>) segV.getListaCharacteristicsSegV();

	    for (CharacteristicsSegV carSegV : listaCarSegV) {

		pstmt.setString(1, segV.getNSC());
		pstmt.setInt(2, Integer.valueOf(segV.getNIIN()));
		pstmt.setString(3, segV.getInc());
		pstmt.setString(4, carSegV.getMrc());
		pstmt.setString(5, carSegV.getMc());
		pstmt.setString(6, carSegV.getRc());
		pstmt.setString(7, (carSegV.getSaic() != 0) ? String.valueOf(carSegV.getSaic()) : null);
		pstmt.setString(8, carSegV.getSac());
		pstmt.setString(9, carSegV.getRaw());

		pstmt.addBatch();
	    }

	    // Create an int[] to hold returned values
	    num = pstmt.executeBatch();

	    // Explicitly commit statements to apply changes
	    this.conn.commit();

	    // limpa o objeto stmt
	    pstmt.clearBatch();

	} catch (BatchUpdateException e) {

	    // System.err.println("SQLException: " + e.getMessage());
	    // System.err.println("SQLState: " + e.getSQLState());
	    // System.err.println("Message: " + e.getMessage());
	    // System.err.println("Vendor: " + e.getErrorCode());
	    // System.err.print("Update counts: ");
	    // int[] updateCounts = e.getUpdateCounts();
	    // for (int i = 0; i < updateCounts.length; i++) {
	    // System.err.print(updateCounts[i] + " ");
	    // }

//	    int[] updateCount = e.getUpdateCounts();
//
//	    int count = 1;
//	    for (int i : updateCount) {
//		if (i == Statement.EXECUTE_FAILED) {
//		    System.out.println("Error on request " + count + ": Execute failed");
//		} else {
//		    System.out.println("Request " + count + ": OK");
//		}
//		count++;
//
//	    }

	    if (LOGGER.isErrorEnabled()) {
		LOGGER.error("[BatchUpdateException] " + e.getMessage());
	    }

	    num = e.getUpdateCounts();
	    // throw e;
	} catch (SQLException e) {
	    if (LOGGER.isErrorEnabled()) {
		LOGGER.error("[SQLException] " + e.getMessage());
	    }
	    // throw e;
	} finally {
	    pstmt.close();
	    pool.liberarConnection(this.conn);

	}

	// Long cont = 0L;

	for (int i = 0; i < num.length; i++) {
	    if (num[i] == -2) {
		// System.out.println("A query " + i + " n�o afetou nenhuma linha");
		cont++;
	    }
	}

	return cont;

    }

    /**
     * Faz Insert no BD por lista de SegV e suas caracteristicas, por meio de uma
     * Procedure
     * 
     * @deprecated - Demora muito para buscar a decodificacao nas funcoes e inserir
     *             na tabela decodificado.
     * 
     * @param -
     *            lista de Objeto <SegmentV>
     * @return - tota de querys inseridas no BD com sucesso.
     * @throws SQLException
     */
    public Long insertBatchProcedure1(List<SegmentV> lista) throws SQLException, BatchUpdateException
    {
	Long cont = 0L;

	for (SegmentV segV : lista) {

	    // try {

	    cont += insertProcedureOneSegV(segV);

	    // } catch (BatchUpdateException e) {
	    // throw e;
	    // }
	}

	return cont;

    }

    /**
     * Insere um SegV + a lista de caracteristicas do segmento
     * 
     * @deprecated - Demora muito para buscar a decodificacao nas funcoes e inserir
     *             na tabela decodificado.
     * 
     * @param SegmentV
     * @return
     * @throws SQLException
     * 
     * @example: NIIN: 000010079 NSC: 4720 INC_NRO: 20311 INC: D20311# INC_RAW:
     *           NAMED20311# ListaReplayCode: [AAGRLA1, AAJD1BA2B, AAJD1CA1A, ...]
     *           ListaCaracteristica: [ ----CharacteristicsSegV----- MRC: AAGR
     *           ModeCode: L Saic: 0 Sac: null RespostaCode: A1 Raw: AAGRLA1#,
     *           ----CharacteristicsSegV----- ... ]
     **/
    public Long insertProcedureOneSegV(SegmentV segV) throws SQLException, BatchUpdateException
    {

	// File fileErros = new File(System.getProperty("user.home") + File.separator +
	// "segV-erros-BD.txt");
	// fileErros.delete();// deleta o arquivo caso exista.
	// FileWriter frErros = null;
	// BufferedWriter bufferErros = null;

	// LocalDateTime agora = LocalDateTime.now();
	// DateTimeFormatter formatador =
	// DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
	// .withLocale(new Locale("pt", "br"));
	// agora.format(formatador); //08/04/14 10:02

	int[] num = null;
	Long cont = 0L;
	// Set auto-commit to false
	this.conn.setAutoCommit(true);
	String insertSTP = "{call STP_CARGA_RAW_SEG_V(?,?,?,?,?,?,?,?,?)}";
	CallableStatement pstmt = conn.prepareCall(insertSTP);

	try {

	    // frErros = new FileWriter(fileErros);
	    // bufferErros = new BufferedWriter(frErros);

	    List<CharacteristicsSegV> listaCarSegV = (List<CharacteristicsSegV>) segV.getListaCharacteristicsSegV();

	    for (CharacteristicsSegV carSegV : listaCarSegV) {
		pstmt.setString(1, segV.getNSC());
		pstmt.setInt(2, Integer.valueOf(segV.getNIIN()));
		pstmt.setString(3, segV.getInc());
		pstmt.setString(4, carSegV.getMrc());
		pstmt.setString(5, carSegV.getMc());
		pstmt.setString(6, carSegV.getRc());
		pstmt.setString(7, String.valueOf(carSegV.getSaic()));
		pstmt.setString(8, carSegV.getSac());
		pstmt.setString(9, carSegV.getRaw());

		pstmt.addBatch();
	    }

	    // Create an int[] to hold returned values
	    num = pstmt.executeBatch();

	    // Explicitly commit statements to apply changes
	    this.conn.commit();

	    // limpa o objeto stmt
	    pstmt.clearBatch();

	} catch (BatchUpdateException e) {

	    // System.err.println("SQLException: " + e.getMessage());
	    // System.err.println("SQLState: " + e.getSQLState());
	    // System.err.println("Message: " + e.getMessage());
	    // System.err.println("Vendor: " + e.getErrorCode());
	    // System.err.print("Update counts: ");
	    // int[] updateCounts = e.getUpdateCounts();

	    // int[] updateCount = e.getUpdateCounts();
	    //
	    // int count = 1;
	    // for (int i : updateCount) {
	    // if (i == Statement.EXECUTE_FAILED) {
	    // System.out.println("Error on request " + count + ": Execute failed");
	    // } else {
	    // System.out.println("Request " + count + ": OK");
	    // }
	    // count++;
	    //
	    // }

	    // gravo o a linha do item que teve problemas na gravacao no arquivo
	    // bufferErros.write(agora.format(formatador) + segV.getNIIN() + segV.getNSC()
	    // + segV.getRawCharacteristicsGroup() + "\n");

	    // for (int i = 0; i < updateCount.length; i++) {
	    // System.err.print(updateCount[i] + " ");
	    // }

	    if (LOGGER.isErrorEnabled()) {
		LOGGER.error("[BatchUpdateException] " + e.getMessage());
	    }

	    num = e.getUpdateCounts();
	    // throw e;
	} catch (SQLException e) {
	    // System.err.println("[2� catch SQLException] Erro: " + e.getMessage());
	    if (LOGGER.isErrorEnabled()) {
		LOGGER.error("[SQLException] " + e.getMessage());
	    }
	    // throw e;
	} finally {
	    if (pstmt != null)
		pstmt.close();

	    pool.liberarConnection(this.conn);

	    for (int i = 0; i < num.length; i++) {
		if (num[i] >= 0) {
		    cont++;
		}
	    }

	    // bufferErros.close();
	    // frErros.close();
	}

	return cont;

    }

    public int getCountItens()
    {
	PreparedStatement ps = null;
	ResultSet rs = null;

	String sqlSelect = "SELECT COUNT (ID) AS TOTAL FROM NATOCHARCODE";

	try {
	    ps = this.conn.prepareStatement(sqlSelect);
	    rs = ps.executeQuery();

	    if (rs.next()) {

		//System.out.println(rs.getString("TOTAL"));
		return rs.getInt("TOTAL");
	    }

	    rs.close();
	    ps.close();

	} catch (SQLException e) {
	    // if (LOGGER.isErrorEnabled()) {
	    LOGGER.error("[SQLException] " + e.getMessage());
	    // }
	} finally {
	    pool.liberarConnection(this.conn);
	}

	return 0;
    }
    
    public int getCountItensBuscarPorNiin(String valor)
    {
	PreparedStatement ps = null;
	ResultSet rs = null;

	String sqlSelect = "SELECT COUNT (ID) AS TOTAL FROM NATOCHARCODE WHERE niin LIKE ?"; //LIKE '%?%'";

	try {
	    ps = this.conn.prepareStatement(sqlSelect);
	    ps.setString(1, valor);
	    
	    rs = ps.executeQuery();

	    if (rs.next()) {

		// System.out.println(rs.getString("TOTAL"));
		return rs.getInt("TOTAL");
	    }

	    rs.close();
	    ps.close();

	} catch (SQLException e) {
	    // if (LOGGER.isErrorEnabled()) {
	    LOGGER.error("[SQLException] " + e.getMessage());
	    // }
	} finally {
	    pool.liberarConnection(this.conn);
	}

	return 0;
    }
    
    public List<SegmentoV> buscarPorNiin(int linhaInicial, int linhaFinal, String valor)
    {
	String sql = "SELECT s.*\n" + 
		"    FROM (SELECT ROWNUM AS NRLINHA, s.*\n" + 
		"            FROM (SELECT *\n" + 
		"                    FROM (  --query para buscar todos o segmentos V\n" + 
		"                          SELECT /*+ FIRST_ROWS */ nc.*\n" + 
		"                            FROM NATOCHARCODE nc--order by niin\n" + 
		"                         )\n" + 
		"                   WHERE ROWNUM <= ? \n" + 
		"                     AND niin LIKE ? ) s) s\n" + 
		"   WHERE (? <= NRLINHA)\n" + 
		"ORDER BY NRLINHA";
	List<SegmentoV> retorno = new ArrayList<>();
	try {
	    PreparedStatement stmt = this.conn.prepareStatement(sql);
	    stmt.setInt(1, linhaFinal);
	    stmt.setString(2, valor);
	    stmt.setInt(3, linhaInicial);

	    ResultSet resultado = stmt.executeQuery();
	    while (resultado.next()) {
		SegmentoV s = new SegmentoV();
		s.setId(resultado.getInt("NRLINHA"));
		s.setNsc(resultado.getString("CLASSE"));
		s.setNIIN(resultado.getString("NIIN"));
		s.setInc(resultado.getString("INC"));
		s.setMrc(resultado.getString("MRC"));
		s.setSaic(resultado.getString("SAIC"));
		s.setSac(resultado.getString("SAC"));
		s.setRc(resultado.getString("RESP_COD"));
		s.setRaw(resultado.getString("RC_RAW"));
		s.setMc(resultado.getString("MC"));

		retorno.add(s);
	    }
	} catch (SQLException e) {
	    // Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
	    // if (LOGGER.isErrorEnabled()) {
	    LOGGER.error("[SQLException] " + e.getMessage());
	    // }
	}
	return retorno;
    }

    public List<SegmentoV> listar(int linhaInicial, int linhaFinal)
    {
	String sql = "SELECT s.*\n" 
		+ "    FROM (SELECT ROWNUM AS NRLINHA, s.*\n" 
		+ "            FROM (SELECT *\n"
		+ "                    FROM ( \n" 
		+ "                          SELECT /*+ FIRST_ROWS */ nc.*\n"
		+ "                            FROM NATOCHARCODE nc  order by nc.niin\n" 
		+ "                         )\n"
		+ "                   WHERE ROWNUM <= ? \n" 
		+ "                 ) s\n" 
		+ "         ) s\n"
		+ "   WHERE (? <= NRLINHA)\n" + "ORDER BY NRLINHA";
	List<SegmentoV> retorno = new ArrayList<>();
	try {
	    PreparedStatement stmt = this.conn.prepareStatement(sql);
	    stmt.setInt(1, linhaFinal);
	    stmt.setInt(2, linhaInicial);

	    ResultSet resultado = stmt.executeQuery();
	    while (resultado.next()) {
		SegmentoV s = new SegmentoV();
		s.setId(resultado.getInt("NRLINHA"));
		s.setNsc(resultado.getString("CLASSE"));
		s.setNIIN(resultado.getString("NIIN"));
		s.setInc(resultado.getString("INC"));
		s.setMrc(resultado.getString("MRC"));
		s.setSaic(resultado.getString("SAIC"));
		s.setSac(resultado.getString("SAC"));
		s.setRc(resultado.getString("RESP_COD"));
		s.setRaw(resultado.getString("RC_RAW"));
		s.setMc(resultado.getString("MC"));

		retorno.add(s);
	    }
	} catch (SQLException e) {
	    // Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
	    // if (LOGGER.isErrorEnabled()) {
	    LOGGER.error("[SQLException] " + e.getMessage());
	    // }
	}
	return retorno;
    }
    
    public SegmentoV listarResultDecode(String niin, String mrc)
    {
	String sql = "SELECT /*+ FIRST_ROWS */  F_DECODIFICA_FIIG (SUBSTR (nc.INC, 2)) vFIIG,\n" + 
		"       F_DECODIFICA_MRC (nc.MRC) vMRC_DECODE,\n" + 
		"       F_CLEAR_REPLY (nc.INC || '#',\n" + 
		"                      SUBSTR (nc.INC, 2),\n" + 
		"                      nc.MRC,\n" + 
		"                      nc.MC || nc.RESP_COD || '#')\n" + 
		"          vCLEAR_REPLY_DECODE\n" + 
		"  FROM NATOCHARCODE nc\n" + 
		" WHERE 1 = 1 \n" + 
		"  AND nc.NIIN = ?\n" + 
		"  AND NC.MRC = ?";
	//List<SegmentoV> retorno = new ArrayList<>();
	try {
	    PreparedStatement stmt = this.conn.prepareStatement(sql);
	    stmt.setString(1, niin);
	    stmt.setString(2, mrc);

	    ResultSet resultado = stmt.executeQuery();
	    
	    if (resultado.next()) {
		SegmentoV s = new SegmentoV();
		s.setFiig(resultado.getString("VFIIG"));
		s.setDecodeMRC(resultado.getString("VMRC_DECODE"));
		s.setDecodeRC(resultado.getString("VCLEAR_REPLY_DECODE"));

		return s;
		//retorno.add(s);
	    }
	} catch (SQLException e) {
	    // Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
	    // if (LOGGER.isErrorEnabled()) {
	    LOGGER.error("[SQLException] " + e.getMessage());
	    // }
	}
	
	return null;
    }

    public List<SegV> listarSegV(int linhaInicial, int linhaFinal)
    {
	String sql = "SELECT s.*\n" 
		+ "    FROM (SELECT ROWNUM AS NRLINHA, s.*\n" 
		+ "            FROM (SELECT *\n"
		+ "                    FROM ( \n" 
		+ "                          SELECT /*+ FIRST_ROWS */ nc.*\n"
		+ "                            FROM NATOCHARCODE nc\n" 
		+ "                         )\n"
		+ "                   WHERE ROWNUM <= ? \n" 
		+ "                 ) s\n" 
		+ "         ) s\n"
		+ "   WHERE (? <= NRLINHA)\n" 
		+ "ORDER BY NRLINHA";
	List<SegV> retorno = new ArrayList<>();
	try {
	    PreparedStatement stmt = this.conn.prepareStatement(sql);
	    stmt.setInt(1, linhaFinal);
	    stmt.setInt(2, linhaInicial);

	    ResultSet resultado = stmt.executeQuery();
	    while (resultado.next()) {
		SegV s = new SegV();
		s.setId(resultado.getInt("NRLINHA"));
		s.setNsc(resultado.getString("CLASSE"));
		s.setNiin(resultado.getString("NIIN"));
		s.setInc(resultado.getString("INC"));
		s.setMrc(resultado.getString("MRC"));
		s.setSaic(resultado.getString("SAIC"));
		s.setSac(resultado.getString("SAC"));
		s.setRc(resultado.getString("RESP_COD"));
		s.setRaw(resultado.getString("RC_RAW"));
		s.setMc(resultado.getString("MC"));

		retorno.add(s);
	    }
	} catch (SQLException e) {
	    // if (LOGGER.isErrorEnabled()) {
	    LOGGER.error("[SQLException] " + e.getMessage());
	    // }
	}
	return retorno;
    }

    /**
     * Limpa a tabela no BD
     * 
     * @return int de Statement.executeUpdate
     * @throws SQLException
     */
    public int truncate() throws SQLException
    {
	Statement ps = null;

	String sqlSelect = "TRUNCATE TABLE NATOCHARCODE";
	int rs;

	try {
	    ps = this.conn.createStatement();
	    rs = ps.executeUpdate(sqlSelect);
	    this.conn.commit();
	    System.out.println(rs);
	    ps.close();

	} finally {
	    pool.liberarConnection(this.conn);
	}

	return rs;
    }
    

    public int insertAtualizacao() throws SQLException
    {

	this.conn.setAutoCommit(true);
	String insertSql = "INSERT INTO SEGV_DATA_ATUALIZACAO (DATA_DO_RAW, MANUTENCAO, QTD_REGISTROS, QTD_LINHAS, PROGRAMA) "
		+ "VALUES (?,?,?,?,?)";
	PreparedStatement pstmt = conn.prepareStatement(insertSql);

	try {
	    
	    // Explicitly commit statements to apply changes
	    this.conn.commit();

	    // limpa o objeto stmt
	    pstmt.clearBatch();

	}  catch (SQLException e) {
	    // System.err.println("[2� catch SQLException] Erro: " + e.getMessage());
	    if (LOGGER.isErrorEnabled()) {
		LOGGER.error("[SQLException] " + e.getMessage());
	    }
	    // throw e;
	} finally {
	    pstmt.close();
	    pool.liberarConnection(this.conn);

	}

	return 0;

    }

}
