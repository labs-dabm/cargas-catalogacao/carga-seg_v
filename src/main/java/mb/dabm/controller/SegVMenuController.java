package mb.dabm.controller;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

public class SegVMenuController implements Initializable
{
    @FXML
    private VBox vbox;

    @FXML
    private AnchorPane root;

    @FXML
    private JFXButton menuClean;

    @FXML
    private JFXButton menuExtract;

    @FXML
    private JFXButton menuView;

    @FXML
    private JFXButton menuExit;

    @FXML
    private AnchorPane anchorPaneVersao;

    @FXML
    private Label lblVersao;

    @FXML
    private Label lblDivisao;

    @FXML
    private Label lblTime;
    
    private int minute;
    private int hour;
    private int second;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1)
    {
	initTimeLine();
	//setVersao("Versão: 2.0.9 - 05/02/2018");
	
//	lblDivisao.setText("Autor: 1T(T) Anders - DAbM-625");
    }
    
    /**
     * https://stackoverflow.com/questions/42383857/javafx-live-time-and-date
     * http://blog.caelum.com.br/conheca-a-nova-api-de-datas-do-java-8/
     * http://www.baeldung.com/current-date-time-and-timestamp-in-java-8
     */
    private void initTimeLine()
    {

	LocalDate hoje = LocalDate.now();
	DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	hoje.format(formatador); // 08/04/2014

	Timeline clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {
	    Calendar cal = Calendar.getInstance();
	    second = cal.get(Calendar.SECOND);
	    minute = cal.get(Calendar.MINUTE);
	    hour = cal.get(Calendar.HOUR_OF_DAY);
	    // System.out.println(hour + ":" + (minute) + ":" + second);
	    lblTime.setStyle("-fx-text-fill: #065fb8; -fx-font-size: 13;");
	    lblTime.setText("Data: " + hoje.format(formatador) + " - " + hour + ":" + (minute) + ":" + second);
	}), new KeyFrame(Duration.seconds(1)));
	clock.setCycleCount(Animation.INDEFINITE);
	clock.play();
    }
    
    public void setVersao(String ver)
    {
	lblVersao.setStyle("-fx-text-fill: #065fb8; -fx-font-size: 13;");
	lblVersao.setText(ver);
    }
    
    public void setAutor(String ver)
    {
	lblDivisao.setText(ver);
    }

}
