package mb.dabm.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.util.StringConverter;
import mb.dabm.dao.InfoProgramaDAO;
import mb.dabm.dao.SegmentVDAO;
import mb.dabm.database.IPool;
import mb.dabm.database.Pool;
import mb.dabm.helper.Helper;
import mb.dabm.model.SegmentoV;

public class ListarSegV2Controller implements Initializable
{
    @FXML
    private AnchorPane root;

    @FXML
    private JFXButton btnBuscar;

    @FXML
    private Label lblNIIN;

    @FXML
    private Label lblNIIN1;

    @FXML
    private JFXTextField txtNIIN;

    @FXML
    private JFXTextField txtNinnBD;

    @FXML
    private Pagination pagination;

    @FXML
    private JFXButton btnLoadData;

    @FXML
    private AnchorPane anchorPaneDecode;

    @FXML
    private AnchorPane anchorPaneTableView;

    @FXML
    private TextArea txtClearRC;

    @FXML
    private Label lblTxtClearRc;

    @FXML
    private JFXTextField txtDecodeMRC;

    @FXML
    private Label lblDecodeMRC;

    @FXML
    private Label lblFIIG;

    @FXML
    private JFXTextField txtFIIG;

    @FXML
    private JFXComboBox<Label> combo;

    private IPool pool = Pool.getInstacia();
    private final SegmentVDAO segVDAO = new SegmentVDAO(pool);
    private final InfoProgramaDAO infoDao = new InfoProgramaDAO(pool);

    private Integer totalRows;

    int pageCount = 5;
    int currentPageIndex = 0;

    private int rowsPerPage = 1000;

    private ObservableList<SegmentoV> dataList = FXCollections
	    .observableArrayList(new ArrayList<SegmentoV>(rowsPerPage));

    StackPane tablePane = new StackPane();
    private TableView<SegmentoV> table = null;
    private ProgressIndicator progressIndicator = new ProgressIndicator();
    
    private final Tooltip tooltip = new Tooltip();
    private static final Logger LOGGER = LogManager.getLogger(ListarSegV2Controller.class.getName());

    @Override
    public void initialize(URL arg0, ResourceBundle arg1)
    {

	// System.out.println("initialize: " + getClass());

	table = initializeTable(dataList);

	tablePane.setVisible(false);
	progressIndicator.setMaxSize(200, 200);
	tablePane.getChildren().add(table);
	tablePane.getChildren().add(progressIndicator);
	anchorPaneTableView.getChildren().add(tablePane);

	pagination.setVisible(false);

	btnBuscar.disableProperty().bind(txtNinnBD.textProperty().isEmpty());

	table.getSelectionModel().selectedItemProperty()
		.addListener((observable, oldValue, newValue) -> selecionarItemTableViewSegV(newValue));

	initCombobox();
		
	initTooltipPesquisaRapida();
	//System.out.println(totalRows);

    }

    /**
     * 
     */
    private void initTooltipPesquisaRapida()
    {
	tooltip.setText(
	    "O campo pesquisa rápida procura\n"
	    + " o NIIN no itens que estão na tabela\n");
	
	txtNIIN.setTooltip(tooltip);
    }

    private void initCombobox()
    {
	combo.getItems().add(new Label("1000"));
	combo.getItems().add(new Label("5000"));
	combo.getItems().add(new Label("10000"));
	combo.getItems().add(new Label("1000000"));

	combo.getSelectionModel().select(new Label("1000"));

	combo.setEditable(true);
	// combo.setPromptText("Select Java Version");
	combo.setConverter(new StringConverter<Label>() {
	    @Override
	    public String toString(Label object)
	    {
		return object == null ? "" : object.getText();
	    }

	    @Override
	    public Label fromString(String string)
	    {
		return new Label(string);
	    }
	});

	combo.getSelectionModel().selectedItemProperty()
		.addListener((observable, oldValue, newValue) -> alterarRowsPerPage(oldValue, newValue));

    }

    private void alterarRowsPerPage(Label oldValue, Label newValue)
    {
	// System.out.println("Valor por pagina atual: "+ rowsPerPage);
	// System.out.println("Valor por pagina novo: "+ newValue.getText());
	// System.out.println("Valor por pagina anterior: "+ oldValue.getText());

	if (!oldValue.getText().equals(newValue.getText())) {
	    if (newValue != null && Helper.isNumeric(newValue.getText())) {
		rowsPerPage = Integer.valueOf(newValue.getText());

		if (txtNinnBD.getText() != null) {
		    buscarSegmentoPorNiin();
		} else {
		    criar();
		}

	    }
	}

    }

    private void criar()
    {

	// System.out.println("criar");

	txtNinnBD.setText(null);
	txtNIIN.setText(null);

	tablePane.setVisible(true);
	pagination.setCurrentPageIndex(0);
	pagination.setVisible(false);

	// Ouvinte para VRF se houve mudanca de pagina
	pagination.currentPageIndexProperty().addListener(new ChangeListener<Number>() {
	    @Override
	    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue)
	    {
		// System.out.println("Pagination Changed from " + oldValue + " , to " +
		// newValue);
		currentPageIndex = newValue.intValue();

		if (txtNinnBD.getText() != null) {
		    // System.out.println("processar thread busca niin");
		    processarThreadBuscaNiin();
		} else {
		    // System.out.println("processar thread normal");
		    processarThread();
		}

		filtrarDados(); // habilita o filtro na atualizacao dos dados
	    }

	});

	filtrarDados(); // habilita o filtro na atualizacao dos dados

	processarThread();

    }

    private void processarThreadBuscaNiin()
    {
	progressIndicator.setVisible(true);
	dataList.clear();

	// long running background task
	new Thread() {
	    public void run()
	    {

		try {

		    // faz a contagem da linhas a serem buscadas, com base no que foi passado
		    totalRows = segVDAO.getCountItensBuscarPorNiin(txtNinnBD.getText());

		    int fromIndex = (currentPageIndex * rowsPerPage) + 1;

		    // int toIndex = Math.min(fromIndex + rowsPerPage, totalRows);
		    int toIndex = ((currentPageIndex * rowsPerPage + rowsPerPage <= totalRows)
			    ? currentPageIndex * rowsPerPage + rowsPerPage
			    : totalRows);

		    // System.out.println("fromIndex: " + fromIndex);
		    // System.out.println("toIndex: " + toIndex);

		    List<SegmentoV> loadedList = loadDataPorNiin(fromIndex, toIndex, txtNinnBD.getText());

		    Platform.runLater(() -> dataList.setAll(loadedList));

		} finally {

		    Platform.runLater(() -> {
			progressIndicator.setVisible(false);
			pageCount = getPageCount(totalRows, rowsPerPage);
			pagination.setPageCount(pageCount);
			pagination.setVisible(true);
		    });

		}
	    }
	}.start();
    }

    private List<SegmentoV> loadDataPorNiin(int fromIndex, int toIndex, String valor)
    {

	List<SegmentoV> list = new ArrayList<>();

	try {

	    list = segVDAO.buscarPorNiin(fromIndex, toIndex, valor);

	    // Thread.sleep(2000);

	} catch (Exception e) {
	    //e.printStackTrace();
	    LOGGER.error("[Exception] " + e.getMessage());
	}

	return list;
    }

    /**
     * 
     */
    private void processarThread()
    {
	progressIndicator.setVisible(true);
	dataList.clear();

	// long running background task
	new Thread() {
	    public void run()
	    {

		try {

		    totalRows = buscarTotal();

		    int fromIndex = (currentPageIndex * rowsPerPage) + 1;

		    int toIndex = ((currentPageIndex * rowsPerPage + rowsPerPage <= totalRows)
			    ? currentPageIndex * rowsPerPage + rowsPerPage
			    : totalRows);

		    // System.out.println("fromIndex: " + fromIndex);
		    // System.out.println("toIndex: " + toIndex);

		    List<SegmentoV> loadedList = loadData(fromIndex, toIndex);

		    Platform.runLater(() -> dataList.setAll(loadedList));

		} finally {

		    Platform.runLater(() -> {
			progressIndicator.setVisible(false);
			pageCount = getPageCount(totalRows, rowsPerPage);
			pagination.setPageCount(pageCount);
			pagination.setVisible(true);
		    });

		}
	    }
	}.start();
    }

    /**
     * Carrega os dados
     * 
     * @param fromIndex
     * @param toIndex
     * @return
     */
    private List<SegmentoV> loadData(int fromIndex, int toIndex)
    {

	List<SegmentoV> list = new ArrayList<>();

	try {

	    list = segVDAO.listar(fromIndex, toIndex);

	    // Thread.sleep(2000);

	} catch (Exception e) {
	    //e.printStackTrace();
	    LOGGER.error("[Exception] " + e.getMessage());
	}

	return list;
    }

    private void selecionarItemTableViewSegV(SegmentoV segmentoV)
    {

	if (segmentoV != null) {

	    SegmentoV rs = segVDAO.listarResultDecode(segmentoV.getNIIN(), segmentoV.getMrc());
	    segmentoV.setFiig(rs.getFiig());
	    segmentoV.setDecodeMRC(rs.getDecodeMRC());
	    segmentoV.setDecodeRC(rs.getDecodeRC());
	    txtFIIG.setText(rs.getFiig());
	    txtClearRC.setText(rs.getDecodeRC());
	    txtDecodeMRC.setText(rs.getDecodeMRC());
	    anchorPaneDecode.setVisible(true);
	} else {
	    anchorPaneDecode.setVisible(false);
	    txtFIIG.setText("");
	    txtClearRC.setText("");
	    txtDecodeMRC.setText("");
	}

    }

    @FXML
    private void handleOnCarregarDadosSegV(ActionEvent event) throws IOException
    {
	criar();
    }

    /**
     * 
     * Retorna o total de itens na tabela
     * 
     * @return
     */
    private Integer buscarTotal()
    {	
	return infoDao.buscarTotalDadosInseridosTabela();	
	//return segVDAO.getCountItens();
    }

    /**
     * Configura e inicializa a tabela
     */
    @SuppressWarnings("unchecked")
    private TableView<SegmentoV> initializeTable(ObservableList<SegmentoV> data)
    {

	TableView<SegmentoV> table = new TableView<>();
	table.setPrefSize(781.0, 276.0);

	TableColumn<SegmentoV, Integer> tableColumnLinha = new TableColumn<>("id");
	tableColumnLinha.setCellValueFactory(param -> param.getValue().id);
	tableColumnLinha.setPrefWidth(46.0);
	tableColumnLinha.setSortable(false);

	TableColumn<SegmentoV, String> tableColumnNiin = new TableColumn<>("NIIN");
	tableColumnNiin.setCellValueFactory(new PropertyValueFactory<SegmentoV, String>("NIIN"));
	tableColumnNiin.setPrefWidth(85.0);

	TableColumn<SegmentoV, String> tableColumnClasse = new TableColumn<>("nsc");
	tableColumnClasse.setCellValueFactory(new PropertyValueFactory<SegmentoV, String>("nsc"));
	tableColumnClasse.setSortable(false);
	tableColumnClasse.setPrefWidth(60.0);

	TableColumn<SegmentoV, String> tableColumnInc = new TableColumn<>("inc");
	tableColumnInc.setCellValueFactory(new PropertyValueFactory<SegmentoV, String>("inc"));
	tableColumnInc.setSortable(false);
	tableColumnInc.setPrefWidth(51.0);

	TableColumn<SegmentoV, String> tableColumnMrc = new TableColumn<>("mrc");
	tableColumnMrc.setCellValueFactory(new PropertyValueFactory<SegmentoV, String>("mrc"));
	tableColumnMrc.setSortable(false);
	tableColumnMrc.setPrefWidth(55.0);

	TableColumn<SegmentoV, String> tableColumnMc = new TableColumn<>("mc");
	tableColumnMc.setCellValueFactory(new PropertyValueFactory<SegmentoV, String>("mc"));
	tableColumnMc.setSortable(false);
	tableColumnMc.setPrefWidth(35.0);

	TableColumn<SegmentoV, String> tableColumnRc = new TableColumn<>("rc");
	tableColumnRc.setCellValueFactory(new PropertyValueFactory<SegmentoV, String>("rc"));
	tableColumnRc.setSortable(false);
	tableColumnRc.setPrefWidth(372.0);

	TableColumn<SegmentoV, String> tableColumnRaw = new TableColumn<>("raw");
	tableColumnRaw.setCellValueFactory(new PropertyValueFactory<SegmentoV, String>("raw"));
	tableColumnRaw.setSortable(false);
	tableColumnRaw.setPrefWidth(300.0);

	table.getColumns().addAll(tableColumnLinha, tableColumnNiin, tableColumnClasse, tableColumnInc, tableColumnMrc,
		tableColumnMc, tableColumnRc, tableColumnRaw);

	table.setItems(data);

	table.setPlaceholder(new Label(""));

	return table;

    }

    /**
     * Retora a contagem de paginas com base no total
     *
     * @param totalCount
     * @param itemsPerPage
     * @return
     */
    private int getPageCount(int totalCount, int itemsPerPage)
    {
	float floatCount = Float.valueOf(totalCount) / Float.valueOf(itemsPerPage);
	int intCount = totalCount / itemsPerPage;

	return ((floatCount > intCount) ? ++intCount : intCount);
    }

    /**
     * Filtro rapido com base nos dados da tabela Ressalta-se que este filtro nao
     * faz a consulta na BD.
     */
    private void filtrarDados()
    {
	// 1. Wrap the ObservableList in a FilteredList (initially display all data).
	FilteredList<SegmentoV> filteredData = new FilteredList<>(dataList, p -> true);

	// 2. Set the filter Predicate whenever the filter changes.
	txtNIIN.textProperty().addListener((observable, oldValue, newValue) -> {
	    filteredData.setPredicate(seg -> {
		// If filter text is empty, display all persons.
		if (newValue == null || newValue.isEmpty()) {
		    return true;
		}

		// Compare first name and last name of every person with filter text.
		String lowerCaseFilter = newValue.toLowerCase();

		if (seg.getNIIN().toLowerCase().contains(lowerCaseFilter)) {
		    return true; // Filter matches first name.
		} else if (seg.getNsc().toLowerCase().contains(lowerCaseFilter)) {
		    return true; // Filter matches last name.
		}
		return false; // Does not match.
	    });
	});

	// 3. Wrap the FilteredList in a SortedList.
	SortedList<SegmentoV> sortedData = new SortedList<>(filteredData);

	// 4. Bind the SortedList comparator to the TableView comparator.
	sortedData.comparatorProperty().bind(table.comparatorProperty());

	// funciona, mas da erro c/paginacao
	table.setItems(filteredData);

    }

    @FXML
    private void handleOnBuscarSegV(ActionEvent event) throws IOException
    {
	// System.out.println("handleOnBuscarSegV");

	if (Helper.isNumeric(txtNinnBD.getText())) {
	    buscarSegmentoPorNiin();
	} else {
	    Helper.enviarPergunta(AlertType.WARNING, "Atenção", "Entre com um NIIN válido",
		    "Parâmetro inválido : " + txtNinnBD.getText());

	}

    }

    /**
     * 
     */
    private void buscarSegmentoPorNiin()
    {
	txtNIIN.setText(null);

	if (!txtNinnBD.getText().isEmpty()) {

	    //System.out.println("processar thread busca niin");
	    tablePane.setVisible(true);
	    pagination.setVisible(false);
	    pagination.setCurrentPageIndex(0);

	    filtrarDados(); // habilita o filtro na atualizacao dos dados
	    processarThreadBuscaNiin();

	}
    }

}
