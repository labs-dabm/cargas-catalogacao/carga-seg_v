package mb.dabm.controller;

import java.net.URL;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.effects.JFXDepthManager;

//import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class LoginController implements Initializable
{

    // @FXML
    // private Text closeIcon;
    @FXML
    private AnchorPane cardPane;
    // @FXML
    // private MaterialDesignIconView closeStage;
    @FXML
    private JFXTextField txLogin;
    @FXML
    private JFXPasswordField txSenha;
    @FXML
    private JFXButton btEntrar;
    @FXML
    private Label lblError;

    private Stage stage1 = null;

    private static final Logger LOGGER = LogManager.getLogger(LoginController.class.getName());

    // private Properties props = getApplicationProperties();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
	JFXDepthManager.setDepth(cardPane, 2);
	// acoes pelo teclado
	actionKeyPress();

    }

    private void actionKeyPress()
    {
	txLogin.setOnKeyPressed(k -> {
	    final KeyCombination ENTER = new KeyCodeCombination(KeyCode.ENTER);
	    if (ENTER.match(k)) {
		txSenha.requestFocus();
	    }
	});

	txSenha.setOnKeyPressed(k -> {
	    final KeyCombination ENTER = new KeyCodeCombination(KeyCode.ENTER);
	    if (ENTER.match(k)) {
		this.handleButtonEntrar(null);
	    }
	});
    }

    @FXML
    private void closeStage(MouseEvent event)
    {
	Platform.exit();
    }

    /**
     * Resp. pela autenticacao simples no sistema.
     * 
     * @param event
     */
    @FXML
    private void handleButtonEntrar(ActionEvent event)
    {
	if (txLogin.getText().equals("admin") && txSenha.getText().equals("admin")) {
	    try {
		// System.out.println("Passou");
		chamarTelaPrincipal();
		closeAction();
	    } catch (Exception e) {
		// e.printStackTrace();
		LOGGER.error("[Exception] " + e.getMessage());
	    }
	} else {
	    lblError.setText("Erro na entrada de dados!");
	    lblError.setStyle("-fx-text-fill: red; -fx-font-size: 13;");
	    // System.out.println("Nao Passou");
	}
    }

    /**
     * Resp. para abrir a tela principal do sistema
     * 
     * @throws Exception
     */
    private void chamarTelaPrincipal() throws Exception
    {
	// Instancio a classe FXMLLoader
	FXMLLoader loader = new FXMLLoader();

	// Seta o FXML e Controller no Location da Classe FXMLLoader
	loader.setLocation(MainController.class.getResource("/view/MainSegV.fxml"));

	// Carrega o layout FXML
	AnchorPane root = (AnchorPane) loader.load();

	// Cria a cena
	Scene scene = new Scene(root);

	// Cria a janela principal
	Stage mainScene = new Stage();

	// Define parametros para a janela
	mainScene.setMinWidth(800);
	mainScene.setMinHeight(600);
	mainScene.setResizable(false);
	// mainScene.initStyle(StageStyle.UNDECORATED);
	mainScene.initStyle(StageStyle.DECORATED);
	// Atrela a cena a janela
	mainScene.setScene(scene);

	// setando o arquivo properties no MainController
	MainController controller = loader.getController();

	// String versao = props.getProperty("app.versao");
	// String data = props.getProperty("app.data");
	// controller.setVersao("Versão: 2.0.1321313 - 05/02/2018");
	// setVersao(versao + " - " + data);
	controller.setUsuario(txLogin.getText());

	// Exibe a janela
	mainScene.show();

	// Parent root1 =
	// FXMLLoader.load(getClass().getResource("/mb/dabm/application/Main.fxml"));
	// Scene scene1 = new Scene(root1, 600, 400);
	// Stage mainScene = new Stage();
	// scene1.getStylesheets().add(getClass().getResource("/mb/dabm/application/application.css").toExternalForm());
	// mainScene.setScene(scene1);
	// mainScene.initStyle(StageStyle.UNDECORATED);
	// mainScene.show();
    }

    private void closeAction()
    {
	stage1 = (Stage) btEntrar.getScene().getWindow();
	stage1.close();
    }

}
