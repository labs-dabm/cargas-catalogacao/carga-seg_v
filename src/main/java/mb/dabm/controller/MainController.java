package mb.dabm.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import mb.dabm.dao.InfoProgramaDAO;
import mb.dabm.database.IPool;
import mb.dabm.database.Pool;
import mb.dabm.model.InformacoesPrograma;

public class MainController implements Initializable
{

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private JFXHamburger hamburger;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private AnchorPane conteudoAnchorPane;

    @FXML
    private Label lbltotaRowsTable;

    @FXML
    private Label lblDataRaw;

    @FXML
    private Label lblQtdRegistros;

    @FXML
    private Label lblVer;

    private VBox box;

    private HamburgerBackArrowBasicTransition transition;

    private static final Logger LOGGER = LogManager.getLogger(MainController.class.getName());

    private IPool pool = Pool.getInstacia();

    private InfoProgramaDAO infoDAO;

    private InformacoesPrograma info;

    private Properties props = getApplicationProperties();

    private String usuario;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {

	buscarInfoPrograma();

	// implementar para inicializar com o componente
	try {

	    // Instancio a classe FXMLLoader
	    FXMLLoader loader = new FXMLLoader();

	    // Seta o FXML e Controller no Location da Classe FXMLLoader
	    loader.setLocation(SegVMenuController.class.getResource("/view/SegVMenu.fxml"));

	    // Carrega o layout FXML
	    box = (VBox) loader.load();

	    drawer.setSidePane(box);

	    SegVMenuController controller = loader.getController();
	    controller.setVersao(props.getProperty("app.versao") + " - " + props.getProperty("app.data"));
	    // controller.setAutor("Autor: 1T(T) Anders - DAbM-625");
	    controller.setAutor(props.getProperty("app.autor") + " - " + props.getProperty("app.divisao"));

	    selecionarMenu();

	} catch (IOException e) {
	    LOGGER.error("[IOException] " + e.getMessage());
	}

	actionMenu();

	// setVersao("Versão: 2.0.9 - 05/02/2018");
	setVersao(props.getProperty("app.versao") + " - " + props.getProperty("app.data"));
    }

    public Properties getApplicationProperties()
    {
	Properties props = new Properties();
	InputStream is = getClass().getResourceAsStream("/application.properties");
	try {
	    props.load(is);
	    is.close();
	} catch (IOException e) {
	    // e.printStackTrace();
	    LOGGER.error("[IOException] " + e.getMessage());
	}

	// String versao = props.getProperty("app.versao");
	// String data = props.getProperty("app.data");
	// String autor = props.getProperty("app.autor");
	// String divisao = props.getProperty("app.divisao");

	// setVersao(versao + " - " + data);
	// lblDivisao.setText("Autor: " + autor + " - " + divisao);

	return props;
    }

    public void setApplicationProperties(Properties props)
    {
	this.props = props;
    }

    public void setVersao(String ver)
    {
	lblVer.setStyle("-fx-text-fill: #065fb8; -fx-font-size: 13;");
	lblVer.setText(ver);
    }

    private void buscarInfoPrograma()
    {
	infoDAO = new InfoProgramaDAO(pool);
	info = infoDAO.buscar();

	Locale meuLocal = new Locale("pt", "BR");
	NumberFormat nfVal = NumberFormat.getIntegerInstance(meuLocal);

	lblDataRaw.setStyle("-fx-text-fill: #A52A2A; -fx-font-size: 13;");
	// DateFormat df = new SimpleDateFormat("MM/yyyy");
	// String text = df.format(info.getDataRawData());
	lblDataRaw.setText("Data de Atualização: " + info.getDataMes());

	lblQtdRegistros.setStyle("-fx-text-fill: #A52A2A; -fx-font-size: 13;");
	lblQtdRegistros.setText("Total de Itens: " + nfVal.format(info.getQtdRegistros()));

	lbltotaRowsTable.setStyle("-fx-text-fill: #A52A2A; -fx-font-size: 13;");
	lbltotaRowsTable.setText("Total Registros na Tabela: " + nfVal.format(info.getQtdRowsTabela()));

    }

    /**
     * habilita animacao Menu Left
     */
    private void actionMenu()
    {
	// HamburgerBackArrowBasicTransition transition = new
	// HamburgerBackArrowBasicTransition(hamburger);
	transition = new HamburgerBackArrowBasicTransition(hamburger);

	drawer.setOverLayVisible(false);

	// exibe drawer
	drawer.setOnDrawerOpening((e) -> {
	    // final Transition animation = hamburger.getAnimation();
	    transition.setRate(1);
	    transition.play();
	});

	// oculta drawer
	drawer.setOnDrawerClosing((e) -> {
	    transition.setRate(-1);
	    transition.play();
	});

	// habilita animacao com o hamburger
	hamburger.addEventHandler(MouseEvent.MOUSE_PRESSED, (e) -> {
	    if (drawer.isShown() || drawer.isShowing()) {
		drawer.close();
		drawer.toFront();
	    } else {
		drawer.open();
		drawer.toFront();
	    }

	});

	hamburger.setCursor(Cursor.HAND);
    }

    private void selecionarMenu()
    {
	// HamburgerBackArrowBasicTransition transition = new
	// HamburgerBackArrowBasicTransition(hamburger);

	for (Node node : box.getChildren()) {
	    if (node.getAccessibleText() != null) {
		node.addEventHandler(MouseEvent.MOUSE_CLICKED, (e) -> {

		    transition.setRate(transition.getRate() * -1);
		    transition.playFrom(new Duration(1000));

		    switch (node.getAccessibleText()) {
			case "menu_limpeza":
			    handleMenuLimpeza();
			    break;
			case "menu_extracao":
			    handleMenuExtracao();
			    break;
			case "menu_lista":
			    handleMenuLista();

			    break;
			case "menu_sair":
			    closeApplication();

		    }
		});
	    }
	}

	
    }

    /**
     * 
     */
    private void handleMenuLimpeza()
    {
	drawer.close();
	drawer.setOnDrawerClosed((e) -> {
	    try {
		AnchorPane a = (AnchorPane) FXMLLoader.load(getClass().getResource("/view/TelaLimpezaSegV.fxml"));
		conteudoAnchorPane.getChildren().setAll(a);
		drawer.toBack();

	    } catch (IOException e1) {

		LOGGER.error("[IOException] " + e1.getMessage());
	    }
	});

    }

    /**
     * 
     */
    private void handleMenuExtracao()
    {
	drawer.close();
	drawer.setOnDrawerClosed((e) -> {
	    try {
		// AnchorPane a = (AnchorPane)
		// FXMLLoader.load(getClass().getResource("/view/TelaExtracaoSegV.fxml"));
		// conteudoAnchorPane.getChildren().setAll(a);
		// drawer.toBack();

		//
		// Instancio a classe FXMLLoader
		FXMLLoader loader = new FXMLLoader();
		// Seta o FXML e Controller no Location da Classe FXMLLoader
		loader.setLocation(ExtracaoSegVController.class.getResource("/view/TelaExtracaoSegV.fxml"));
		//
		AnchorPane page = (AnchorPane) loader.load();

		//
		conteudoAnchorPane.getChildren().setAll(page);

		ExtracaoSegVController controller = loader.getController();
		// controller.getTotal();
		if (controller.isBtnConfirmarClicked()) {
		    buscarInfoPrograma();
		}

		drawer.toBack();

	    } catch (IOException e1) {

		// e1.printStackTrace();
		LOGGER.error("[IOException] " + e1.getMessage());
	    }
	});

	// anchorPane.setBackground(
	// new Background(new BackgroundFill(Paint.valueOf("#0288D1"),
	// CornerRadii.EMPTY, Insets.EMPTY)));
    }

    /**
     * 
     */
    private void handleMenuLista()
    {

	drawer.close();
	drawer.setOnDrawerClosed((e) -> {
	    try {
		AnchorPane a = (AnchorPane) FXMLLoader.load(getClass().getResource("/view/TelaViewSegV1.fxml"));
		conteudoAnchorPane.getChildren().setAll(a);
		drawer.toBack();
	    } catch (IOException e1) {
		// e1.printStackTrace();
		LOGGER.error("[IOException] " + e1.getMessage());
		// Helper.enviarAlerta(AlertType.ERROR, "Erro", "N�o foi poss�vel abrir a Tela",
		// e1.getMessage());
	    }
	});
    }

    @FXML
    public void closeApplication()
    {
	Platform.exit();
    }

    public Properties getProps()
    {
	return props;
    }

    public void setProps(Properties props)
    {
	this.props = props;
    }

    public String getUsuario()
    {
	return usuario;
    }

    public void setUsuario(String usuario)
    {
	// this.usuario = new SimpleStringProperty(usuario);
	this.usuario = usuario;
	// System.out.println("valor:" +this.usuario + "-" + usuario);
    }

    /**
     * Resp. para chamar a tela do Segmento V
     * 
     * @throws IOException
     */
    // @FXML
    // public void handleMenuItemSegV() throws IOException
    // {
    // AnchorPane a = (AnchorPane)
    // FXMLLoader.load(getClass().getResource("/view/SegV.fxml"));
    // anchorPane.getChildren().setAll(a);
    // }

}
