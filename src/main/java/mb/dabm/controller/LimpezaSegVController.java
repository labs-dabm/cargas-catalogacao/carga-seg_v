package mb.dabm.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Date;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXProgressBar;
import com.jfoenix.controls.JFXTextField;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import mb.dabm.dao.InfoProgramaDAO;
import mb.dabm.database.IPool;
import mb.dabm.database.Pool;
import mb.dabm.helper.Helper;
import mb.dabm.helper.Timer;
import mb.dabm.model.InformacoesPrograma;

public class LimpezaSegVController implements Initializable
{
    @FXML
    private AnchorPane root;

    @FXML
    private JFXButton btnSelectFile;

    @FXML
    private JFXButton btnVrfFile;

    @FXML
    private JFXTextField txtNameFile;

    @FXML
    private JFXProgressBar progress;

    @FXML
    private JFXListView<File> listaViewFilesSplit;

    @FXML
    private Label lblStatus;

    @FXML
    private JFXButton btnTask;

    @FXML
    private AnchorPane anchorPaneResumo;

    @FXML
    private Label lblTotalItensFile;

    @FXML
    private JFXTextField txtTotalItensFile;

    @FXML
    private Label lblTotalItensContinuacao;

    @FXML
    private JFXTextField txtotalItensContinuacao;

    @FXML
    private Label lblTotalLinhas;

    @FXML
    private JFXTextField txtTotalLinhas;

    @FXML
    private Label lblTempoProc;

    @FXML
    private JFXTextField txtTempoProc;

    @FXML
    private Label lblItensProblema;

    @FXML
    private JFXButton btnItensProblema;

    //
    private File arq;
    private Task<Object> copyWorker;
    private BooleanProperty editMode = new SimpleBooleanProperty();
    private BooleanProperty modeTask = new SimpleBooleanProperty();
    private Long totalLinhas;
    private Long totalLinhasArquivo;
    private Locale meuLocal = new Locale("pt", "BR");
    private NumberFormat nfVal = NumberFormat.getIntegerInstance(meuLocal);
    private Task<Object> splitWorker;
    private ObservableList<File> observableListFile;
    private List<File> listaSplitFile;
    private BooleanProperty dividirFile = new SimpleBooleanProperty();
    private InformacoesPrograma info = new InformacoesPrograma();
    private InfoProgramaDAO infDAO;
    private IPool pool = Pool.getInstacia();

    private static final Logger LOGGER = LogManager.getLogger(LimpezaSegVController.class.getName());

    @Override
    public void initialize(URL arg0, ResourceBundle arg1)
    {

	modeTask.set(true);
	// ao iniciar a var inicia como false e para disable o btn inverte seu valor
	btnTask.disableProperty().bind(modeTask);
	// so habiliata o btn se o campo textServico for =! de null ou txtLogin =! null
	btnVrfFile.disableProperty().bind(txtNameFile.textProperty().isEmpty());
	progress.setProgress(0);

	// System.out.println(modeTask);
	// String dir = System.getProperty("user.home") + File.separator + "carga_seg_v"
	// + File.separator + "partes";
	carregarLista();
    }

    @FXML
    public void handleClickFile(MouseEvent event)
    {
	File file = listaViewFilesSplit.getSelectionModel().getSelectedItem();

	if (file != null) {
	    Optional<ButtonType> result = Helper.enviarPergunta(AlertType.CONFIRMATION, "Abrir Arquivo",
		    "Selecionado o arquivo: " + file.getName(), "Deseja abrir?");

	    if (result.get() == ButtonType.OK) {
		Helper.openFile(file);
	    }
	}

    }

    private void carregarLista()
    {

	// String diretorio = "D:\\DAbM\\CDs-RAW-DATA\\partes";
	String dir = System.getProperty("user.home") + File.separator + "carga_seg_v" + File.separator + "partes";
	// System.out.println(dir);
	// Path rootPath = Paths.get(System.getProperty("user.home") + File.separator +
	// "carga_seg_v" + File.separator + "partes");

	File file = new File(dir);

	if (!file.isDirectory()) {
	    try {
		FileUtils.forceMkdir(file);
	    } catch (IOException e) {
		// e.printStackTrace();
		LOGGER.error("[IOException] " + e.getMessage());
	    }
	}

	// File file = new File(diretorio);
	File afile[] = file.listFiles();

	listaSplitFile = new ArrayList<>();

	int i = 0;
	for (int j = afile.length; i < j; i++) {
	    File arquivos = afile[i];
	    // System.out.println(arquivos.getAbsolutePath());
	    listaSplitFile.add(arquivos);
	}

	listaSplitFile.sort(new Comparator<File>() {

	    @Override
	    public int compare(File o1, File o2)
	    {
		if (o1.getName().length() == o2.getName().length()) {
		    return o1.compareTo(o2);
		}
		return o1.getName().length() - o2.getName().length();
	    }
	});

	observableListFile = FXCollections.observableArrayList(listaSplitFile);
	listaViewFilesSplit.setItems(observableListFile);

	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	listaViewFilesSplit.setCellFactory(new Callback<ListView<File>, ListCell<File>>() {

	    @Override
	    public ListCell<File> call(ListView<File> param)
	    {
		ListCell<File> cell = new ListCell<File>() {

		    @Override
		    protected void updateItem(File t, boolean bln)
		    {
			super.updateItem(t, bln);
			if (t != null) {
			    setText(t.getAbsoluteFile() + " ( Modificado: " + sdf.format(t.lastModified()) + " "
				    + "Tamanho: " + FileUtils.byteCountToDisplaySize(t.length()) + ")");
			}
		    }

		};

		return cell;
	    }
	});

    }

    /**
     * Processo para fazer a varredura noa arquivo
     * 
     * @param fileName
     * @return
     */
    private Task<Object> createWorkerVerificarErros(String fileName)
    {
	return new Task<Object>() {
	    @Override
	    protected Object call() throws Exception
	    {
		File fileErros = new File(System.getProperty("user.home") + File.separator + "carga_seg_v"
			+ File.separator + "CargaSegV_error.txt");
		// String dir = System.getProperty("user.home") + File.separator + "carga_seg_v"
		// + File.separator + "partes";

		fileErros.delete();// deleta o arquivo caso exista.
		FileWriter frErros = null;
		BufferedWriter bufferErros = null;

		Long nro = 0L;
		Long nroLinha = 0L;
		Long nroLinhatraco = 0L;
		String nsnAnterior = "";
		Long nroLinhaRawMenor = 0L;
		String temp = "";
		Timer timer = new Timer();
		String current = null;

		try (BufferedReader br = new BufferedReader(
			new InputStreamReader(new FileInputStream(fileName), "windows-1252"))) {

		    frErros = new FileWriter(fileErros);
		    bufferErros = new BufferedWriter(frErros);

		    modeTask.set(false);
		    btnVrfFile.disableProperty().bind(editMode.not());
		    btnSelectFile.disableProperty().bind(editMode.not());
		    while (true) {

			current = br.readLine();
			if (current == null) {
			    editMode.set(true);
			    modeTask.set(true);
			    btnVrfFile.disableProperty().bind(editMode.not());
			    btnSelectFile.disableProperty().bind(editMode.not());
			    // System.out.println("terminou 1");
			    break;
			}

			// para cancelar a execucao
			if (isCancelled()) {
			    progress.progressProperty().unbind();
			    progress.setProgress(0);
			    // System.out.println("cancelou 1");
			    break;
			}
			// updateProgress((nro + 1), -1);
			// updateMessage("Task part " + nfVal.format(nro + 1) + " complete");
			// Thread.sleep(10); // 28/11/17

			String niin = current.substring(0, 9);
			String nsc = current.substring(9, 13);

			if (nsnAnterior.isEmpty()) {
			    nsnAnterior = current.substring(0, 13);
			}

			// se o final de linha for '-' e o NSN anterior for igual ao corrente, entao a
			// proxima linha ainda pertence ao mesmo NSN e assim por diante...
			if (current.endsWith("-") && nsnAnterior.equals(niin + nsc)) {

			    // temp += current.substring(13);
			    temp += current.substring(13, current.length() - 1);
			    nroLinhatraco++;

			} else if (current.endsWith("##")) {

			    temp += current.substring(13);
			    // System.out.println("temp no ##: "+ temp + "\n");
			    //String linhaMontada = niin + nsc + temp;

			    // anders 05/01/2018
			    List<String> lista = new ArrayList<String>(Arrays.asList(temp.split("#")));
			    List<String> result = new ArrayList<>();
			    for (String code : lista) {

				String v = code.trim();
				String carMRC = v.substring(0, 1);

				// System.out.println(Helper.isAlphabeticAndLengh4("-AJD"));

				if ((Helper.isAlphabeticAndLengh4(v.substring(0, 4)) == false
					&& !carMRC.equalsIgnoreCase("9"))
					|| (v.length() < 4 && !carMRC.equalsIgnoreCase("9"))) {

				    // System.out.println("nao passou: " + v);
				    result.add(v + "#");
				} // else {
				  // System.out.println("passou: " + v);
				  // }

			    }

			    if (result.size() > 0) {
				bufferErros.write(niin + nsc + result.toString() + "\n");
				// bufferErros.write("linha montada: " + linhaMontada + "\n");
				bufferErros.write("linha corrente: " + current + "\n");
				nroLinhaRawMenor++;
				result = new ArrayList<>();
			    }

			    // anders

			    // so para testa depois remover
//			    if (linhaMontada.substring(13).replace("##", "").length() < 4) {
//				// System.out.println("Linha: " + nroLinha + " - " + current);
//				bufferErros.write(linhaMontada + "\n");
//				nroLinhaRawMenor++;
//			    }

			    updateProgress((nro + 1), -1);
			    // updateMessage("Task part " + String.valueOf(nro + 1) + " complete");
			    updateMessage("Task part " + nfVal.format(nro + 1) + " complete");
			    // Thread.sleep(10); // 28/11/17

			    // System.out.println(linhaMontada);

			    nro++;
			    temp = "";
			    nsnAnterior = "";
			}

			// System.out.println(current);

			// System.out.println("linha:" + nfVal.format(nroLinha));
			nroLinha++;

		    }

		    updateMessage("Processo finalizado: " + nfVal.format(nroLinha) + " Linhas lidas!");

		    // System.out.println("-----Metodo: verificarErros----");

		    // System.out.println("Linhas c/##: " + nro);
		    totalLinhas = nro;
		    txtTotalItensFile.setText(nfVal.format(nro));
		    txtTotalItensFile.setStyle("-fx-text-fill: red; -fx-font-size: 13;");

		    // System.out.println("Linhas c/-: " + nroLinhatraco);
		    txtotalItensContinuacao.setText(nfVal.format(nroLinhatraco));
		    txtotalItensContinuacao.setStyle("-fx-text-fill: red; -fx-font-size: 13;");

		    // System.out.println("Linhas Lida do Arquivo Principal: " + nroLinha);
		    txtTotalLinhas.setText(nfVal.format(nroLinha));
		    txtTotalLinhas.setStyle("-fx-text-fill: red; -fx-font-size: 13;");
		    totalLinhasArquivo = nroLinha;

		    // System.out.println("Tempo de Processamento: " + timer);
		    txtTempoProc.setText(timer.toString());
		    txtTempoProc.setStyle("-fx-text-fill: red; -fx-font-size: 13;");

		    // txtTotQuerys.setText(nfVal.format(nroLinhaRawMenor));
		    // txtTotQuerys.setStyle("-fx-text-fill: red; -fx-font-size: 13;");
		    // System.out.println("Linhas com raw menor 6: " + nroLinhaRawMenor);

		    // System.out.println("Anders: " + Helper.isAlphabeticAndLengh4("9007"));

		    // System.out.println("-----FIM Metodo: verificarErros----");

		} catch (IOException e) {
		    // e.printStackTrace();
		    LOGGER.error("[IOException] " + e.getMessage());
		    throw e;

		} catch (Exception ex) {
		    // ex.printStackTrace();
		    LOGGER.error("[Exception] " + ex.getMessage());

		    throw ex;
		} finally {
		    try {

			bufferErros.close();
			frErros.close();

			// verficacoes para saber se o arquivo tem conteudo
			if (fileErros.length() > 0) {
			    lblItensProblema.setVisible(true);
			    btnItensProblema.setVisible(true);
			    btnItensProblema.setOnAction(e -> Helper.openFile(fileErros));
			} else {
			    lblItensProblema.setVisible(false);
			    btnItensProblema.setVisible(false);
			}

		    } catch (IOException e) {
			// e.printStackTrace();
			LOGGER.error("[IOException] " + e.getMessage());
			updateMessage("Houve um erro no processamento!");
			throw e;
		    }

		    updateMessage("Houve um erro no processamento!");
		}

		return true;
	    }

	};
    }

    /**
     * nro: 3 Metodo para processar a leitura do arquivo selecionado no metodo
     * 'btnAbrirClickFile'. Esse medot irah delegar a responsabilidade para o metodo
     * 'verificaErros', a fim de ver se o arquivo possui erros de padronizacao de
     * leitura.
     * 
     * @param event
     * @throws IOException
     */
    @FXML
    private void onProcessarLeitura(ActionEvent event) throws IOException
    {
	copyWorker = createWorkerVerificarErros(this.arq.getPath());

	progress.progressProperty().unbind();
	progress.progressProperty().bind(copyWorker.progressProperty());
	lblStatus.textProperty().bind(copyWorker.messageProperty());

	// atribui uma acao para o button, caso seja press botao parar.
	btnTask.setOnAction(e -> {
	    if (copyWorker.isRunning()) {
		// System.out.println("valor copyWorker - getState: " + copyWorker.getState());
		// //RUNNING ou CANCELLED
		// System.out.println("cancelou 3");
		editMode.set(true);
		modeTask.set(true);
		btnVrfFile.disableProperty().bind(editMode.not());
		btnSelectFile.disableProperty().bind(editMode.not());

		progress.progressProperty().unbind();
		progress.setProgress(0);
		copyWorker.cancel();
	    }

	});

	// caso ocorra alguma excecao na Task
	copyWorker.setOnFailed(evt -> {
	    // System.out.println("Task failed!");
	    if (copyWorker.getException() instanceof IndexOutOfBoundsException) {
		// System.out.println("...with an IndexOutOfBoundsException");
		LOGGER.error("Task failed! ...with an IndexOutOfBoundsException");
	    } else if (copyWorker.getException() instanceof NumberFormatException) {
		// System.out.println("...with a NumberFormatException");
		LOGGER.error("Task failed! ...with a NumberFormatException");
	    } else {
		// System.out.println("...with another, unexpected execption");
		LOGGER.error("Task failed! ...with another, unexpected execption");
	    }

	    Optional<ButtonType> result = Helper.enviarPergunta(AlertType.ERROR, "Ocorreu um Erro",
		    "Houve um erro na verificação do arquivo!",
		    "Mensagem: " + copyWorker.getException().getLocalizedMessage());

	    if (result.get() == ButtonType.OK) {

		editMode.set(true);
		modeTask.set(true);
		btnVrfFile.disableProperty().bind(editMode.not());
		btnSelectFile.disableProperty().bind(editMode.not());
		progress.progressProperty().unbind();
		progress.setProgress(0);
	    }

	    // System.out.println(copyWorker.getException().getLocalizedMessage());
	    // System.out.println(copyWorker.getException().getCause());
	    // System.out.println(copyWorker.getException().getStackTrace());

	});

	// caso ocorra a leitura com sucesso
	copyWorker.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
	    @Override
	    public void handle(WorkerStateEvent t)
	    {

		progress.progressProperty().unbind();
		progress.setProgress(1);

		Optional<ButtonType> result = Helper.enviarPergunta(AlertType.CONFIRMATION, "Sucesso",
			"Arquivo analisado com sucesso!", "Deseja dividir o aquivo?");

		listaViewFilesSplit.getItems().clear();
		if (result.get() == ButtonType.OK) {
		    dividirFile.set(true);
		} else {
		    dividirFile.set(false);
		}
		onSplitToFile();

	    }
	});

	new Thread(copyWorker).start();

    }

    /**
     * Cria o Processo para fazer a divisao do arquivo em arquivos menores
     * 
     * @param event
     * @throws IOException
     */
    private void onSplitToFile()
    {
	// splitWorker
	splitWorker = createWorkerSplitFile(this.arq.getPath());

	progress.progressProperty().unbind();
	progress.progressProperty().bind(splitWorker.progressProperty());
	lblStatus.textProperty().bind(splitWorker.messageProperty());

	// atribui uma acao para o button, caso seja press botao parar.
	btnTask.setOnAction(e -> {
	    if (splitWorker.isRunning()) {
		// System.out.println("valor copyWorker - getState: " + copyWorker.getState());
		// //RUNNING ou CANCELLED
		// System.out.println("cancelou 3");
		editMode.set(true);
		modeTask.set(true);
		btnVrfFile.disableProperty().bind(editMode.not());
		btnSelectFile.disableProperty().bind(editMode.not());

		progress.progressProperty().unbind();
		progress.setProgress(0);
		splitWorker.cancel();
	    }

	});

	// caso ocorra alguma excecao na Task
	splitWorker.setOnFailed(evt -> {
	    // System.out.println("Task failed!");
	    // if (copyWorker.getException() instanceof IndexOutOfBoundsException) {
	    // System.out.println("...with an IndexOutOfBoundsException");
	    // } else if (copyWorker.getException() instanceof NumberFormatException) {
	    // System.out.println("...with a NumberFormatException");
	    // } else {
	    //
	    // System.out.println("...with another, unexpected execption");
	    //
	    // }

	    Optional<ButtonType> result = Helper.enviarPergunta(AlertType.ERROR, "Ocorreu um Erro",
		    "Houve um erro na verificação do arquivo!",
		    "Mensagem: " + copyWorker.getException().getLocalizedMessage());

	    if (result.get() == ButtonType.OK) {

		editMode.set(true);
		modeTask.set(true);
		btnVrfFile.disableProperty().bind(editMode.not());
		btnSelectFile.disableProperty().bind(editMode.not());
		progress.progressProperty().unbind();
		progress.setProgress(0);

	    }

	    // System.out.println(copyWorker.getException().getLocalizedMessage());
	    // System.out.println(copyWorker.getException().getCause());
	    // System.out.println(copyWorker.getException().getStackTrace());

	});

	// caso ocorra a leitura com sucesso
	splitWorker.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
	    @Override
	    public void handle(WorkerStateEvent t)
	    {

		progress.progressProperty().unbind();
		progress.setProgress(1);

		Helper.enviarAlerta(AlertType.INFORMATION, "Sucesso", "Arquivo analisado com sucesso!",
			"Total de itens analisados no arquivo: " + nfVal.format(totalLinhas));

		atulizarDadosInfo();

		// System.out.println("processo 2 finalizado");
		observableListFile.clear();
		listaViewFilesSplit.getItems().clear();

		// String dir = System.getProperty("user.home") + File.separator + "carga_seg_v"
		// + File.separator
		// + "partes";
		carregarLista();
		// carregarLista();// carrega a lista com os novo arquivos
		editMode.set(true);
		modeTask.set(true);
		btnVrfFile.disableProperty().bind(editMode.not());
		btnSelectFile.disableProperty().bind(editMode.not());
	    }
	});

	new Thread(splitWorker).start();
    }

    private void atulizarDadosInfo()
    {
	LocalDate hoje = LocalDate.now();
	// DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	Date date = Date.valueOf(hoje);

	info.setDataRawData(date);
	info.setManutencao("F");
	info.setQtdLinhas(0);
	info.setQtdRegistros(totalLinhas);
	info.setQtdLinhas(totalLinhasArquivo);

	infDAO = new InfoProgramaDAO(pool);

	infDAO.updateInfo(info);
    }

    /**
     * Executa o Processo para fazer a divisao do arquivo em arquivos menores
     * 
     * @param path
     * @return
     */
    private Task<Object> createWorkerSplitFile(String fileName)
    {
	return new Task<Object>() {
	    @Override
	    protected Object call() throws Exception
	    {

		Long nro = 0L;
		Long nroLinha = 0L;
		Long nroLinhatraco = 0L;
		String nsnAnterior = "";
		Timer timer = new Timer();
		String temp = "";
		// String current = null;
		Long nol = 1000000L; // No. of lines to be split and saved in each output file.
		// String diretorio = "D:\\DAbM\\CDs-RAW-DATA\\partes";
		String dir = System.getProperty("user.home") + File.separator + "carga_seg_v" + File.separator
			+ "partes";
		// Helper.deleteDirectory(new File(diretorio)); //right way to remove directory
		// in Java

		// File file = new File(diretorio);
		File file = new File(dir);

		FileUtils.forceMkdir(file);
		FileUtils.cleanDirectory(file);

		int nof = 1;
		if (dividirFile.getValue()) {

		    double tempLines = (totalLinhas / nol);

		    int temp1 = 0;
		    if (totalLinhas < nol) {
			temp1 = 1;
			tempLines = 1;
		    } else {
			temp1 = (int) tempLines;
		    }

		    nof = 0;
		    if (temp1 == tempLines) {
			nof = temp1;
		    } else {
			nof = temp1 + 1;
		    }

		} else {
		    nol = totalLinhas;
		}

		// System.out.println("No. of files to be generated :" + nof); // Displays no.
		// of files to be generated.

		try {

		    modeTask.set(false);
		    btnVrfFile.disableProperty().bind(editMode);
		    btnSelectFile.disableProperty().bind(editMode);

		    FileInputStream fstream = new FileInputStream(fileName);
		    DataInputStream in = new DataInputStream(fstream);

		    BufferedReader br = new BufferedReader(new InputStreamReader(in));
		    String strLine;

		    for (int j = 1; j <= nof; j++) {
			FileWriter fstream1 = new FileWriter(dir + File.separator + "segv_p" + j + ".txt"); // Destination
			// File
			// Location
			// se for dividir entao progresso
			if (dividirFile.getValue()) {
			    updateProgress(j, nof);
			    updateMessage("Arquivo parte " + nfVal.format(j) + " complete");
			    // Thread.sleep(10); // 28/11/17
			}

			BufferedWriter out = new BufferedWriter(fstream1);
			for (int i = 1; i <= nol; i++) {

			    // se nao for para dividir o arquivo
			    if (!dividirFile.getValue()) {
				updateProgress(i, nol);
				updateMessage("Linha parte " + nfVal.format(i) + " complete");
				// Thread.sleep(10); // 28/11/17
			    }
			    strLine = br.readLine();

			    if (strLine != null) {

				String niin = strLine.substring(0, 9);
				String nsc = strLine.substring(9, 13);

				if (nsnAnterior.isEmpty()) {
				    nsnAnterior = strLine.substring(0, 13);
				}

				// se o final de linha for '-' e o NSN anterior for igual ao corrente, entao a
				// proxima linha ainda pertence ao mesmo NSN e assim por diante...
				if (strLine.endsWith("-") && nsnAnterior.equals(niin + nsc)) {

				    // temp += strLine.substring(13);
				    temp += strLine.substring(13, strLine.length() - 1);
				    nroLinhatraco++;

				} else if (strLine.endsWith("##")) {

				    temp += strLine.substring(13);

				    String linhaMontada = niin + nsc + temp;
				    out.write(linhaMontada);
				    // out.write("linha: "+ strLine);

				    nro++;
				    temp = "";
				    nsnAnterior = "";

				    if (i != nol) {
					out.newLine();
				    }

				}

				// System.out.println("linha:" + nfVal.format(nroLinha));
				nroLinha++;

			    } // fim if (strLine != null)

			} // fim segundo for
			out.close();
		    } // fim primeiro for

		    if (dividirFile.getValue()) {
			updateMessage(nof + " arquivos gerado(s) pelo sistema.");
		    } else {
			updateMessage(nof + " arquivo gerado pelo sistema. Total: " + nfVal.format(nol));
		    }

		    // System.out.println(dividirFile.getValue());
		    // System.out.println(dividirFile.get());

		    // System.out.println("-----Metodo: verificarErros----");

		    // System.out.println("Tempo de Processamento: " + timer);
		    txtTempoProc.setText(timer.toString());
		    txtTempoProc.setStyle("-fx-text-fill: blue; -fx-font-size: 13;");

		    // System.out.println("-----FIM Metodo: verificarErros----");

		    in.close();

		} catch (Exception ex) {
		    // ex.printStackTrace();
		    LOGGER.error("[Exception] " + ex.getMessage());

		    throw ex;
		}

		return true;
	    }

	};
    }

    /**
     * nro 1 Metodo para localizar o arquivo a ser lido
     * 
     * @param event
     * @throws IOException
     */
    @FXML
    private void btnAbrirClickFile(ActionEvent event) throws IOException
    {

	Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
	FileChooser fileChooser = new FileChooser();

	// Set extension filter
	FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TEXT files (*.txt)", "*.txt");
	fileChooser.getExtensionFilters().add(extFilter);

	// para testes pode comentar a linha abaixo (disable para teste - anders)
	configureFileChooser(fileChooser, "Selecione o Arquivo");

	// para testes pode descomentar as linhas abaixo, a fim de facilitar a abertura
	// do arquivo
	// fileChooser.setTitle("Seleicone o Arquivo");
	// File defaultDirectory = new File("./test");
	// fileChooser.setInitialDirectory(defaultDirectory);

	arq = fileChooser.showOpenDialog(stage);

	if (arq != null) {
	    txtNameFile.setText("Arquivo Selecionado: " + arq.getName());
	}

    }

    /**
     * nro 2 Responsavel para setar um titulo e um diretorio inicial para localizar
     * o arquivo
     * 
     * @param fileChooser
     * @param nameTitle
     */
    private static void configureFileChooser(final FileChooser fileChooser, String nameTitle)
    {
	fileChooser.setTitle(nameTitle);
	fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
    }

}
