package mb.dabm.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXProgressBar;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import mb.dabm.dao.InfoProgramaDAO;
import mb.dabm.dao.SegmentVDAO;
import mb.dabm.database.IPool;
import mb.dabm.database.Pool;
import mb.dabm.helper.Helper;
import mb.dabm.helper.Timer;
import mb.dabm.model.SegmentV;

public class ExtracaoSegVController implements Initializable
{
    @FXML
    private AnchorPane root;

    @FXML
    private JFXButton btnExtractFile;

    @FXML
    private JFXProgressBar progress;

    @FXML
    private Label lblStatus;

    @FXML
    private JFXButton btnTask;

    @FXML
    private JFXButton btnCalcularTotal;

    @FXML
    private AnchorPane anchorPaneResumo;

    @FXML
    private AnchorPane anchorListaView;

    @FXML
    private Label lblTotalItensFile;

    @FXML
    private JFXTextField txtTotalItensFile;

    @FXML
    private Label lblTotalQuerys;

    @FXML
    private JFXTextField txtTotalQuerys;

    @FXML
    private Label lblTotalLinhas;

    @FXML
    private JFXTextField txtTotalLinhas;

    @FXML
    private Label lblTempoProc;

    @FXML
    private JFXTextField txtTempoProc;

    @FXML
    private Label lblItensErro;

    @FXML
    private JFXButton btnItensErro;

    @FXML
    private Label lblQtdQuerys;

    @FXML
    private JFXTextField txtQtdQuerys;

    @FXML
    private Label lblBancoDados;

    @FXML
    private JFXRadioButton radioPROD;

    @FXML
    private JFXRadioButton radioCATALOG;

    @FXML
    private JFXListView<File> listaViewFilesSplit;

    @FXML
    private JFXListView<String> listaViewFilesDelete;

    @FXML
    private Label lblListaArquivos;

    @FXML
    private Label lblListaArquivosDeletados;

    private BooleanProperty editMode = new SimpleBooleanProperty();
    // private BooleanProperty btnMode = new SimpleBooleanProperty();
    private SegmentVDAO segVDAO;
    private IPool pool = Pool.getInstacia();
    private final InfoProgramaDAO infoDao = new InfoProgramaDAO(pool);
    // private Long totalLinhas;
    private ObservableList<File> observableListFile;
    private List<File> listaSplitFile;

    private ObservableList<String> delectedItem = FXCollections.observableArrayList();
    // private final ListView<String> listaFilesDelete = new
    // ListView<>(delectedItem);

    // private File fileResume;
    // private FileWriter frResume = null;
    // private BufferedWriter bufferResume = null;
    private OutputStreamWriter bufferResume = null;

    private Locale meuLocal = new Locale("pt", "BR");
    private NumberFormat nfVal = NumberFormat.getIntegerInstance(meuLocal);
    // private Long totalDaListaFiles = 0L;
    // private Long totalQuerysDaListaFiles = 0L;

    private static final Logger LOGGER = LogManager.getLogger(ExtracaoSegVController.class.getName());

    private ProgressIndicator progressIndicator = new ProgressIndicator();

    private int total;
    
    private boolean btnConfirmarClicked = false;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1)
    {
	initGroupRadio();
	carregarLista();
	initResumeFile(false);// melhorar para o usuario decidir
	// ao iniciar a var inicia como false
	editMode.set(true);
	// para disable o btn inverte seu valor
	btnTask.disableProperty().bind(editMode);
	btnExtractFile.disableProperty()
		.bind(Bindings.isEmpty(listaViewFilesSplit.getSelectionModel().getSelectedItems()));
	// btnExtractFile.disableProperty().bind(editMode.not());
	progress.setProgress(0);

	lblItensErro.setVisible(false);
	btnItensErro.setVisible(false);

	anchorListaView.getChildren().add(progressIndicator);
	progressIndicator.setPrefSize(180, 180);
	progressIndicator.setVisible(false);
	progressIndicator.setLayoutY(25);
	progressIndicator.setLayoutX(200);

	btnCalcularTotal.disableProperty().bind(Bindings.isNotEmpty(listaViewFilesSplit.getItems()));

	listaViewFilesDelete.setItems(delectedItem);
	// btnCalcularTotal.setDisable(false);

    }

    private void initResumeFile(boolean old)
    {
	String dir = System.getProperty("user.home") + File.separator + "carga_seg_v" + File.separator;
	String arquivo = "resume.txt";

	try {

	    bufferResume = new OutputStreamWriter(new FileOutputStream(dir + arquivo, old), "UTF-8");
	} catch (IOException e) {
	    // e.printStackTrace();
	    LOGGER.error("[IOException] " + e.getMessage());
	}

    }

    /**
     * Cria um grupo para o radioButton e atrelar as opcoes neste grupo.
     */
    private void initGroupRadio()
    {
	final ToggleGroup group = new ToggleGroup();
	// Group
	// groupDB = new ToggleGroup();
	radioPROD.setToggleGroup(group);
	radioCATALOG.setToggleGroup(group);
	radioCATALOG.setSelected(true);
    }

    private Long totalArquivo(String fileName)
    {
	// Path path = Paths.get("D:\\DAbM\\CDs-RAW-DATA\\partes\\segv_p11.txt");
	Path path = Paths.get(fileName);
	Charset windowsCharset = Charset.forName("windows-1252");

	long lineCount = 0L;

	try (Stream<String> stream = Files.lines(path, windowsCharset)) {
	    lineCount = stream.count();
	    // System.out.println(lineCount);
	} catch (IOException e) {
	    // e.printStackTrace();
	    LOGGER.error("[IOException] " + e.getMessage());
	}

	return lineCount;
    }

    private void carregarLista()
    {

	// String diretorio = "D:\\DAbM\\CDs-RAW-DATA\\partes";
	String dir = System.getProperty("user.home") + File.separator + "carga_seg_v" + File.separator + "partes";
	File file = new File(dir);
	File afile[] = file.listFiles();

	listaSplitFile = new ArrayList<>();

	int i = 0;
	for (int j = afile.length; i < j; i++) {
	    File arquivos = afile[i];
	    // System.out.println(arquivos.getAbsolutePath());
	    listaSplitFile.add(arquivos);
	}

	listaSplitFile.sort(new Comparator<File>() {

	    @Override
	    public int compare(File o1, File o2)
	    {
		if (o1.getName().length() == o2.getName().length()) {
		    return o1.compareTo(o2);
		}
		return o1.getName().length() - o2.getName().length();
	    }
	});

	observableListFile = FXCollections.observableArrayList(listaSplitFile);
	// SortedList<File> sortedList = new SortedList<File>(observableListFile.);
	// listaViewFilesSplit.setItems(sortedList);
	listaViewFilesSplit.setItems(observableListFile);

	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	listaViewFilesSplit.setCellFactory(new Callback<ListView<File>, ListCell<File>>() {

	    @Override
	    public ListCell<File> call(ListView<File> param)
	    {
		ListCell<File> cell = new ListCell<File>() {

		    @Override
		    protected void updateItem(File t, boolean bln)
		    {
			super.updateItem(t, bln);
			if (t != null) {
			    setText(t.getAbsoluteFile() + " ( Modificado: " + sdf.format(t.lastModified()) + " "
				    + "Tamanho: " + FileUtils.byteCountToDisplaySize(t.length()) + ")");
			}
		    }

		};

		return cell;
	    }
	});

	btnCalcularTotal.disableProperty().bind(Bindings.isNotEmpty(listaViewFilesSplit.getItems()));
	btnExtractFile.disableProperty()
		.bind(Bindings.isEmpty(listaViewFilesSplit.getSelectionModel().getSelectedItems()));

    }

    @FXML
    private void onCalcularTotalRegistrosTable(ActionEvent event)
    {
	// System.out.println(listaViewFilesSplit.getItems());
	// System.out.println(listaViewFilesSplit.getItems().size());

	if (listaViewFilesSplit.getItems().size() == 0) {
	    // System.out.println("vazio");
	    processarThreadTotal();

	} // else {
	  // System.out.println("nao vazio");
	  // }
    }

    private void processarThreadTotal()
    {

	progressIndicator.setVisible(true);
	// long running background task
	new Thread() {
	    public void run()
	    {

		try {

		    segVDAO = new SegmentVDAO(pool);
		    total = segVDAO.getCountItens();
		    infoDao.updateTotalRegistrosNaTabela(total);

		    Platform.runLater(() -> {
			// System.out.println(total);
			Helper.enviarPergunta(AlertType.INFORMATION, "Sucesso", "Cálculo realizado com sucesso",
				"Total de registros na Tabela: " + nfVal.format(total));
		    });

		} finally {

		    Platform.runLater(() -> {
			progressIndicator.setVisible(false);
			btnConfirmarClicked = true;
		    });

		}
	    }
	}.start();
    }

    /**
     * Responsavel por inserir as linhas do segV no BD.
     * 
     * @param event
     * @throws IOException
     */
    @FXML
    private void onProcessarInsercaoSegV(ActionEvent event) throws IOException
    {
	// String arquivo = "D:\\DAbM\\CDs-RAW-DATA\\partes\\segv_p1.txt";
	// String dir = "D:/DAbM/CDs-RAW-DATA/partes/";

	// System.out.println(listaViewFilesSplit.getSelectionModel().getSelectedItems());
	// System.out.println(observableListFile.toString());
	// System.out.println(observableListFile.size());

	// temporario - anders 04/01/18
	if (listaViewFilesSplit.getSelectionModel().getSelectedItem() != null) {

	    File file = listaViewFilesSplit.getSelectionModel().getSelectedItem();
	    // System.out.println(file.getAbsolutePath());
	    // file.delete();
	    // carregarLista();
	    // percorrerUmFile(file);
	    runTask1(file);

	    // disable temporario
	    // ButtonType todos = new ButtonType("Todos", ButtonBar.ButtonData.OK_DONE);
	    // ButtonType um = new ButtonType("Um por vez",
	    // ButtonBar.ButtonData.CANCEL_CLOSE);
	    //
	    // Alert dialog = new Alert(AlertType.CONFIRMATION);
	    // dialog.setTitle("Percorrer Lista");
	    // dialog.setHeaderText("Processar Lista:");
	    // dialog.setContentText("Deseja processar todos os arquivos da Lista?");
	    //
	    // Alert alert = new Alert(AlertType.CONFIRMATION,
	    // "Percorrer Lista " + "Deseja processar todos os arquivos da Lista? ", todos,
	    // um);
	    //
	    // dialog.setTitle("Percorrer Lista");
	    //
	    // Optional<ButtonType> result = alert.showAndWait();
	    //
	    // if (result.isPresent() && result.get() == todos) {
	    // // System.out.println("Todos");
	    // percorrerTodos();
	    // } else {
	    // percorrerUmFile(file);
	    // // System.out.println("um por vez");
	    // }

	}

    }

    private void runTask1(File fileName)
    {
	// se estiver usando o fxml nao precisar iniciar a Classe
	// pois se instancia-la, nao ira funcionar
	// progress = new ProgressBar();
	progress.progressProperty().unbind();
	progress.setProgress(0);
	lblStatus.textProperty().unbind();
	initResumeFile(true);

	Task<Void> longTask = new Task<Void>() {
	    @Override
	    protected Void call() throws Exception
	    {

		// File fileErros = new File(System.getProperty("user.home") + File.separator +
		// "segV-erros-BD.txt");
		// fileErros.delete();// deleta o arquivo caso exista.

		// variaveis para a leitura do arquivo
		// String temp = "";
		String current = null;
		Long nro = 0L;
		Long nroLinha = 0L;
		// Long nroLinhatraco = 0L;
		// Long nroIsac = 0L;
		// String nsnAnterior = "";
		Timer timer = new Timer();
		List<SegmentV> listaSegmentoV = new ArrayList<>();
		long limite = (Helper.isNumeric(txtQtdQuerys.getText())) ? Long.valueOf(txtQtdQuerys.getText()) : 100;
		// System.out.println("Limite por bloco: " + limite);
		long nroQuery = 0;
		long nroQueryIsac = 0;
		long totalQuerys = 0;
		// @SuppressWarnings("unused")
		long totalBlocos = 0;

		try (BufferedReader br = new BufferedReader(
			new InputStreamReader(new FileInputStream(fileName), "windows-1252"))) {

		    // tirar daqui depois
		    LocalDateTime agora = LocalDateTime.now();
		    DateTimeFormatter formatador = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
			    .withLocale(new Locale("pt", "br"));
		    agora.format(formatador); // 08/04/14 10:02

		    bufferResume.write("---Início Leitura do arquivo: '" + fileName.getName() + "' - "
			    + agora.format(formatador) + "---\n");

		    // final Long max = 999796L; // tornar var dianamica
		    Long max = totalArquivo(fileName.getAbsoluteFile().toString());

		    editMode.set(false);
		    btnExtractFile.disableProperty().bind(editMode.not());
		    while (true) {

			current = br.readLine();
			if (current == null) {
			    editMode.set(true);
			    btnTask.disableProperty().bind(editMode);
			    btnExtractFile.disableProperty().bind(editMode.not());

			    break;
			}

			// disable para testes Anders 27/11/17
			// updateProgress((nro + 1), max);
			// updateMessage("Arquivo: " + fileName.getName() + " Parte " + nfVal.format(nro
			// + 1)
			// + " completa de " + max);
			// Thread.sleep(100);

			// Montanodo segmentoV s/isac;
			SegmentV segV = new SegmentV(current);

			// add o obj seg V na lista de Segmentos V
			listaSegmentoV.add(segV);

			// vrf se a lista atingiu o limite estabelecido na tela
			// o default eh 100 itens
			if (listaSegmentoV.size() == limite) {

			    segVDAO = new SegmentVDAO(pool);
			    try {

				// Long linhas = segVDAO.insertBatchProcedure(listaSegmentoV);
				Long linhas = segVDAO.insertListaSegVBatch(listaSegmentoV);
				// Long linhas = segVDAO.insertProcedureOneSegV(segV);

				// System.out.println("linhas inseridas: " + linhas);
				nroQuery += linhas;
				// System.out.println("Querys: " + nroQuery);

			    } catch (SQLException e) {
				// System.err.println("[SQLException] Erro: " + e.getMessage());
				if (LOGGER.isErrorEnabled()) {
				    LOGGER.error("[SQLException] Erro: " + e.getMessage());
				}
			    }

			    // ajustando a barra de progresso na porcentagem
			    updateProgress((nro + listaSegmentoV.size()), max);
			    // ajustando a mensagem para cada loop
			    updateMessage("Tarefa parte " + nfVal.format(nro + listaSegmentoV.size()) + " complete");
			    // ajustando o tempo de leitura por loop, soh para testes
			    // Thread.sleep(10);

			    // para cancelar a execucao
			    if (isCancelled()) {
				// System.out.println("Cancelado Anders");
				updateMessage("Cancelled");
				break;
			    }

			    // atualizando a contagem
			    nro = nro + listaSegmentoV.size();
			    // totalBlocos++;
			    totalBlocos = totalBlocos + 1;
			    // System.out.println("Blocos: " + totalBlocos);
			    // zerando a lista ao atingir o limite
			    listaSegmentoV = new ArrayList<>();
			}

			/*
			 * if(nroLinha == 100) { editMode.set(true);
			 * btnTask.disableProperty().bind(editMode);
			 * btnExtractFile.disableProperty().bind(editMode.not()); break; }
			 */

			// nro++;
			nroLinha++;
			// System.out.println(current);
			// if(nroLinha >= 198540) {
			// System.out.println("linha: " + nroLinha + "- " + current);
			// }

		    }

		    // if (isDone()) {
		    // System.out.println("Finalizado Anders");
		    // }

		    // System.out.println("Blocos no loop: " + totalBlocos);
		    // System.out.println("Querys no loop: " + nroQuery);
		    // System.out.println("QTD de SegV na lista fora do loop: " +
		    // listaSegmentoV.size());
		    // Caso a lista seja menor que o limite entao, ao final da leitura de todo o
		    // arquivo sera gravado o restante da lista no DB, a fim de gravar todos os
		    // dados do segV no BD
		    if (listaSegmentoV.size() > 0) {
			// para um obj do segV ele grava as caracteristicas o BD
			for (SegmentV segV : listaSegmentoV) {

			    segVDAO = new SegmentVDAO(pool);
			    try {

				// Long linhas = segVDAO.insertProcedureOneSegV(segV);
				Long linhas = segVDAO.insertOneSegV(segV);
				nroQuery += linhas;
				// System.out.println("QTD de Inserts por SegV fora do loop: " + linhas);
				totalQuerys = totalQuerys + linhas;

			    } catch (SQLException e) {
				// System.err.println("[SQLException] Erro: " + e.getMessage());
				if (LOGGER.isErrorEnabled()) {
				    LOGGER.error("[SQLException] Erro: " + e.getMessage());
				}
			    }

			    // ajustando a barra de progresso na porcentagem
			    updateProgress((nro + 1), max);
			    // ajustando a mensagem para cada loop
			    updateMessage("Tarefa parte " + nfVal.format(nro + 1) + " complete");
			    // ajustando o tempo de leitura por loop, soh para testes
			    // Thread.sleep(10);
			    // atualiza a leitura e atualizacao do loop na barra de progresso
			    nro++;

			}

			// System.out.println("total de Querys fora do loop: " + totalQuerys);
			// System.out.println("Contagem de nroQuery: " + nroQuery);

		    }

		    bufferResume.write("Linhas c/##: " + nfVal.format(nro) + "\n");
		    // System.out.println("-----Metodo: inserirSegmentoVNoBanco----");
		    // System.out.println("Linhas c/##: " + nro);
		    txtTotalItensFile.setText(nfVal.format(nro));
		    txtTotalItensFile.setStyle("-fx-text-fill: #086aea; -fx-font-size: 13;");

		    // System.out.println("Linhas c/ISAC: " + nroIsac);
		    // txtTotItensISAC.setText(nroIsac.toString());
		    // txtTotItensISAC.setStyle("-fx-text-fill: #086aea; -fx-font-size: 13;");

		    // System.out.println("Linhas c/-: " + nroLinhatraco);
		    // txtTotItensContinuacao.setText(nroLinhatraco.toString());
		    // txtTotItensContinuacao.setStyle("-fx-text-fill: #086aea; -fx-font-size:
		    // 13;");

		    // System.out.println("Linhas Lida do Arquivo Principal: " + nroLinha);
		    bufferResume.write("Linhas Lida do Arquivo Principal: " + nfVal.format(nroLinha) + "\n");
		    txtTotalLinhas.setText(nfVal.format(nroLinha));
		    txtTotalLinhas.setStyle("-fx-text-fill: #086aea; -fx-font-size: 13;");

		    // System.out.println("Tempo de Processamento: " + timer);
		    txtTempoProc.setText(timer.toString());
		    bufferResume.write("Tempo de Processamento: " + timer.toString() + "\n");
		    txtTempoProc.setStyle("-fx-text-fill: #086aea; -fx-font-size: 13;");

		    bufferResume.write("Total de querys: " + nfVal.format(nroQuery + nroQueryIsac) + "\n");
		    txtTotalQuerys.setText(nfVal.format(nroQuery + nroQueryIsac));
		    txtTotalQuerys.setStyle("-fx-text-fill: #086aea; -fx-font-size: 13;");

		    // labelTotalBlocosProc.set("Total de Blocos processados na Carga:"); //
		    // 28/11/17

		    // txtTotalBlocosProc
		    // .setText((totalBlocos > 0 && listaSegmentoV.size() == 0) ?
		    // Long.toString(totalBlocos)
		    // : (totalBlocos > 0) ? Long.toString(totalBlocos) + " + "
		    // : +listaSegmentoV.size() + " Iten(s) do Seg. V");
		    // txtTotalBlocosProc.setStyle("-fx-text-fill: #086aea; -fx-font-size: 13;");
		    // "Arquivo: " + fileName.getName()
		    bufferResume.write("---Fim Leitura do arquivo" + fileName.getName() + "---\n");
		    // System.out.println("-----FIM Metodo: inserirSegmentoVNoBanco----");

		} catch (Exception ex) {
		    // ex.printStackTrace();
		    LOGGER.error("[Exception] " + ex.getMessage());

		    throw ex;

		} finally {

		    try {

			bufferResume.close();

		    } catch (IOException e) {
			// e.printStackTrace();
			LOGGER.error("[IOException] " + e.getMessage());
			throw e;
		    }

		}

		return null;
	    }

	};

	// caso ocorra alguma excecao na Task
	longTask.setOnFailed(evt -> {

	    Optional<ButtonType> result = Helper.enviarPergunta(AlertType.ERROR, "Ocorreu um Erro",
		    "Houve um erro na verificação do arquivo!",
		    "Mensagem: " + longTask.getException().getLocalizedMessage());

	    if (result.get() == ButtonType.OK) {

		progress.progressProperty().unbind();
		progress.setProgress(0);
		lblStatus.textProperty().unbind();

		// longTask.cancel();
		editMode.set(true);
		btnTask.disableProperty().bind(editMode);
		btnExtractFile.disableProperty().bind(editMode.not());

	    }

	    // System.out.println(copyWorker.getException().getLocalizedMessage());
	    // System.out.println(copyWorker.getException().getCause());
	    // System.out.println(copyWorker.getException().getStackTrace());

	});

	// executa MSG de sucesso apos execucao da Thread
	longTask.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
	    @Override
	    public void handle(WorkerStateEvent t)
	    {
		if (fileName.delete()) {
		    // System.out.println(fileName.getName() + " is deleted!");
		    carregarLista();
		    delectedItem.add("Arquivo: " + fileName.getName() + " deletado.");
		}

		Helper.enviarAlerta(AlertType.INFORMATION, "Sucesso",
			"Processo de carga segmento V concluido com sucesso!", "Arquivo atualizado na Base de Dados");
	    }
	});

	// atribui uma acao para o button
	btnTask.setOnAction(e -> {
	    if (longTask.isRunning()) {
		longTask.cancel();
		editMode.set(true);
		btnTask.disableProperty().bind(editMode);
		btnExtractFile.disableProperty().bind(editMode.not());
	    }
	});

	progress.progressProperty().bind(longTask.progressProperty());
	lblStatus.textProperty().bind(longTask.messageProperty());

	new Thread(longTask).start();

    }

    /**
     * @return the btnConfirmarClicked
     */
    public boolean isBtnConfirmarClicked()
    {
        return btnConfirmarClicked;
    }

    /**
     * @param btnConfirmarClicked the btnConfirmarClicked to set
     */
    public void setBtnConfirmarClicked(boolean btnConfirmarClicked)
    {
        this.btnConfirmarClicked = btnConfirmarClicked;
    }
}
