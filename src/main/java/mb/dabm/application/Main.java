package mb.dabm.application;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Main extends Application
{
    
    private static final Logger LOGGER = LogManager.getLogger(Main.class.getName());

    @Override
    public void start(Stage primaryStage)
    {
	try {
	    
	    // Habilitar caso desejar abrir direto a tela que esta trabalhando.
	    // AnchorPane root = (AnchorPane)
	    // FXMLLoader.load(getClass().getResource("/view/SegV.fxml"));
	    // Scene scene = new Scene(root, 600, 371);
	    // scene.getStylesheets().add(getClass().getResource("/mb/dabm/application/bootstrap3.css").toExternalForm());
	    // primaryStage.setScene(scene);
	    // primaryStage.show();
	     
//	    InputStream inputStream = getClass().getClassLoader()
//		        .getResourceAsStream("/config/conexaobd.properties");
	    
	    //habilitar quando estiver em prod, para chamar a tela de login
	    // usu: admin, senha: admin
	    Parent root = FXMLLoader.load(getClass().getResource("/mb/dabm/application/Login.fxml"));
	    Scene scene = new Scene(root, 600, 400);
	    primaryStage.initStyle(StageStyle.TRANSPARENT);
	    primaryStage.setScene(scene);
	    //primaryStage.setResizable(false);
	    primaryStage.show();
	    

	} catch (Exception e) {
	    //e.printStackTrace();
	    LOGGER.error("[Exception] " + e.getMessage());
	}
    }

    public static void main(String[] args)
    {		
	
	// Inicializa o JavaFX
	launch(args);
    }
}
