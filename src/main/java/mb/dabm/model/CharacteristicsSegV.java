package mb.dabm.model;

import mb.dabm.helper.Helper;

public class CharacteristicsSegV
{
    private String mrc;
    private String mc;
    private String rc;
    private int saic;
    private String sac;
    private String raw;

    /*
     * Construtor default
     */
    public CharacteristicsSegV()
    {

    }

    /**
     * Construtor Exemplo de um replayCode: ABJH1AJFM65.0$$JF275.0
     * 
     * @param replayCode
     */
    public CharacteristicsSegV(String replayCode)
    {
	
	//System.out.println(replayCode);

	this.mrc = replayCode.substring(0, 4);
	String isac = replayCode.substring(4, 5);
	
	mc = replayCode.substring(4, 5);
	rc = replayCode.substring(5);
	raw = replayCode + "#";
	
	//System.out.println("-----------INICIO-------------");
	//System.out.println("isac antes: "+isac);
	//System.out.println("mc antes: "+mc);
	//System.out.println("mrc antes: "+mrc);
	//System.out.println("raw antes: "+raw);
	

	if (Helper.isNumeric(isac)) {

	    this.saic = Integer.parseInt(isac);
	    int tam = Integer.parseInt(isac);
	    raw = replayCode + "#";

	    if (replayCode.substring(5).length() >= (5 + tam) + 1) {
		sac = replayCode.substring(5, 5 + tam);
		mc = replayCode.substring((5 + tam), (5 + tam) + 1);
		rc = replayCode.substring((5 + tam) + 1);
	    } else {
		saic = 0;
	    }

	    //System.out.println("MC: " + replayCode.substring((5 + tam), (5 + tam) + 1));
	    //System.out.println("mrc: "+mrc);
	    //System.out.println("MC: " + mc);
	    //System.out.println("SAIC: " + saic);
	    //System.out.println("SAC: " + sac);
	    //system.out.println("-----------FIM-------------");

	} 
    }

    @Override
    public String toString()
    {
	return "CharacteristicsSegV [mrc=" + mrc + ", mc=" + mc + ", rc=" + rc + ", saic=" + saic + ", sac=" + sac
		+ ", raw=" + raw + "]";
    }

    public String getMrc()
    {
	return mrc;
    }

    public void setMrc(String mrc)
    {
	this.mrc = mrc;
    }

    public String getMc()
    {
	return mc;
    }

    public void setMc(String mc)
    {
	this.mc = mc;
    }

    public String getRc()
    {
	return rc;
    }

    public void setRc(String rc)
    {
	this.rc = rc;
    }

    public int getSaic()
    {
	return saic;
    }

    public void setSaic(int saic)
    {
	this.saic = saic;
    }

    public String getSac()
    {
	return sac;
    }

    public void setSac(String sac)
    {
	this.sac = sac;
    }

    public String getRaw()
    {
	return raw;
    }

    public void setRaw(String raw)
    {
	this.raw = raw;
    }

}