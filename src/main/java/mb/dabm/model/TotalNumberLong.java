package mb.dabm.model;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;

public class TotalNumberLong
{
    private LongProperty number;

    public final long getNumber()
    {
	if (number != null) {
	    return number.get();
	}
	return 0;
    }

    public final void setNumber(long number)
    {
	this.numberProperty().set(number);
    }

    public final LongProperty numberProperty()
    {
	if (number == null) {
	    number = new SimpleLongProperty(0);
	}

	return number;
    }
}
