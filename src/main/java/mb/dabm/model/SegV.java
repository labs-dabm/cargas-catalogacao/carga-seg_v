package mb.dabm.model;

public class SegV
{
    private Integer id;
    private String niin;
    private String nsc;
    private String inc;
    private String mrc;
    private String mc;
    private String rc;
    private String saic;
    private String sac;
    private String raw;

    public SegV()
    {

    }

    public SegV(Integer id, String niin, String nsc, String inc, String mrc, String mc, String rc, String saic,
	    String sac, String raw)
    {

	this.id = id;
	this.niin = niin;
	this.nsc = nsc;
	this.inc = inc;
	this.mrc = mrc;
	this.mc = mc;
	this.rc = rc;
	this.saic = saic;
	this.sac = sac;
	this.raw = raw;
    }

    @Override
    public int hashCode()
    {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((niin == null) ? 0 : niin.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj)
    {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	SegV other = (SegV) obj;
	if (niin == null) {
	    if (other.niin != null)
		return false;
	} else if (!niin.equals(other.niin))
	    return false;
	return true;
    }

    @Override
    public String toString()
    {
	return "SegV [id=" + id + ", niin=" + niin + ", nsc=" + nsc + ", inc=" + inc + ", mrc=" + mrc + ", mc=" + mc
		+ ", rc=" + rc + ", saic=" + saic + ", sac=" + sac + ", raw=" + raw + "]";
    }

    public Integer getId()
    {
	return id;
    }

    public void setId(Integer id)
    {
	this.id = id;
    }

    public String getNiin()
    {
	return niin;
    }

    public void setNiin(String niin)
    {
	this.niin = niin;
    }

    public String getNsc()
    {
	return nsc;
    }

    public void setNsc(String nsc)
    {
	this.nsc = nsc;
    }

    public String getInc()
    {
	return inc;
    }

    public void setInc(String inc)
    {
	this.inc = inc;
    }

    public String getMrc()
    {
	return mrc;
    }

    public void setMrc(String mrc)
    {
	this.mrc = mrc;
    }

    public String getMc()
    {
	return mc;
    }

    public void setMc(String mc)
    {
	this.mc = mc;
    }

    public String getRc()
    {
	return rc;
    }

    public void setRc(String rc)
    {
	this.rc = rc;
    }

    public String getSaic()
    {
	return saic;
    }

    public void setSaic(String saic)
    {
	this.saic = saic;
    }

    public String getSac()
    {
	return sac;
    }

    public void setSac(String sac)
    {
	this.sac = sac;
    }

    public String getRaw()
    {
	return raw;
    }

    public void setRaw(String raw)
    {
	this.raw = raw;
    }

}
