package mb.dabm.model;

import java.sql.Date;

public class InformacoesPrograma
{
    private Date dataRawData;
    private String dataMes;
    private String manutencao;
    private long qtdRegistros;
    private long qtdLinhas;
    private long qtdRowsTabela;
    
    private final String programa = "SEGMENTO_V";

    public InformacoesPrograma()
    {

    }

    public InformacoesPrograma(Date dataRawData, String manutencao, long qtdRegistros, long qtdLinhas)
    {
	super();
	this.dataRawData = dataRawData;
	this.manutencao = manutencao;
	this.qtdRegistros = qtdRegistros;
	this.qtdLinhas = qtdLinhas;
    }
    
    public String getDataMes()
    {
        return dataMes;
    }

    public void setDataMes(String dataMes)
    {
        this.dataMes = dataMes;
    }

    public Date getDataRawData()
    {
        return dataRawData;
    }

    public void setDataRawData(Date dataRawData)
    {
        this.dataRawData = dataRawData;
    }

    public String getManutencao()
    {
        return manutencao;
    }

    public void setManutencao(String manutencao)
    {
        this.manutencao = manutencao;
    }

    public long getQtdRegistros()
    {
        return qtdRegistros;
    }

    public void setQtdRegistros(long qtdRegistros)
    {
        this.qtdRegistros = qtdRegistros;
    }

    public long getQtdLinhas()
    {
        return qtdLinhas;
    }

    public void setQtdLinhas(long qtdLinhas)
    {
        this.qtdLinhas = qtdLinhas;
    }

    public String getPrograma()
    {
        return programa;
    }

    @Override
    public int hashCode()
    {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((dataRawData == null) ? 0 : dataRawData.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj)
    {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	InformacoesPrograma other = (InformacoesPrograma) obj;
	if (dataRawData == null) {
	    if (other.dataRawData != null)
		return false;
	} else if (!dataRawData.equals(other.dataRawData))
	    return false;
	return true;
    }

    public long getQtdRowsTabela()
    {
	return qtdRowsTabela;
    }

    public void setQtdRowsTabela(long qtdRowsTabela)
    {
	this.qtdRowsTabela = qtdRowsTabela;
    }
        

}
