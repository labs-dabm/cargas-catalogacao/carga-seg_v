package mb.dabm.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mb.dabm.helper.Helper;

/**
 * 
 * @author carlosanders
 * @link: https://stackoverflow.com/questions/18922165/how-to-include-duplicate-keys-in-hashmap
 */
public class SegmentV
{

    private String NIIN; // 0-9=9
    private String nsc; // 10-13=4
    private String inc;
    private String inc_nro;
    private String inc_raw;
    private String rawCharacteristicsGroup; // comeca line 14 ate 1013
    private List<CharacteristicsSegV> listaCharacteristicsSegV = new ArrayList<>();
    private List<String> listaReplyCode;

    /**
     * Construtor
     */
    public SegmentV()
    {

    }

    /**
     * Construtor com parametros
     * 
     * @param nIIN
     * @param nSC
     */
    public SegmentV(String nIIN, String nSC)
    {
	super();
	this.NIIN = nIIN;
	this.nsc = nSC;
    }

    /**
     * Construtor passando a linha para Iniciar a decodificacao para os campos da
     * tabela
     * 
     * @param line
     * @example: 0000100222815NAMED15884#AGAVGCRAWLER TRACTOR;J I CASE,MODEL
     *           450B.#ALYXL11#FEATGSET OF 4##
     * @example: 0000000428940NAMEECK FILTER ASSEMBLY#AGAVGUSED ON ALM F13
     *           AIRCRAFT##
     */
    public SegmentV(String line)
    {
	this.NIIN = line.substring(0, 9);
	this.nsc = line.substring(9, 13);

	this.rawCharacteristicsGroup = line.substring(13);

	// passa para o metodo tratar a lista
	setListaReplyCode(new ArrayList<String>(Arrays.asList(line.substring(13).split("#"))));

	// depois faz a vrf e add cada MRC como um OBJ CharacteristicsSegV
	this.listaReplyCode.forEach(replayCode -> {
	    // System.out.println(replayCode);
	    CharacteristicsSegV c = new CharacteristicsSegV(replayCode.trim());

	    // Todo segV vem o MRC=NAME, por�m se o MC dele for "D", tem que decodificar
	    // com base nas tabelas do MRD usando as funcoes do Oracle. Mas se For
	    // outro MC o valor serah em texto claro. Desse modo nao terah INC
	    // pois e um novo NAME.

	    if (c.getMrc().equalsIgnoreCase("NAME") && c.getMc().equalsIgnoreCase("D")
	    // colocado pois existe alguns itens que possuem Name, mas com o MC=D
		    && c.getRc().trim().length() == 5) {
		// if (c.getMrc().equalsIgnoreCase("NAME") && c.getMc().equalsIgnoreCase("D")) {
		this.inc = c.getMc() + c.getRc(); // INC: D20311#
		this.inc_raw = c.getRaw(); // INC_RAW: NAMED20311#
		this.inc_nro = c.getRc(); // INC_NRO: 20311
	    }

	    // System.out.println(charSegV.toString());
	    this.listaCharacteristicsSegV.add(c);

	    c = null;
	});

    }

    /**
     * imprime a lista de {@link CharacteristicsSegV}
     */
    public void printListaCaracteristicas()
    {
	this.listaCharacteristicsSegV.forEach(cSegV -> {
	    System.out.println(cSegV.toString());
	});
    }

    /**
     * Representacao textual da Classe
     */
    @Override
    public String toString()
    {
	return "----SEGMENT V-----" + "\nNIIN: " + this.NIIN + "\nNSC: " + this.nsc + "\nINC_NRO: " + this.inc_nro
		+ "\nINC: " + this.inc + "\nINC_RAW: " + this.inc_raw + "\nRawCharacteristicsGroup: "
		+ this.rawCharacteristicsGroup + "\nListaReplayCode: " + this.listaReplyCode + "\nListaCaracteristica: "
		+ this.listaCharacteristicsSegV + "\n----FIM-----";
    }

    // get/set

    public List<CharacteristicsSegV> getListaCharacteristicsSegV()
    {
	return listaCharacteristicsSegV;
    }

    public void setListaCharacteristicsSegV(List<CharacteristicsSegV> listaCharacteristicsSegV)
    {
	this.listaCharacteristicsSegV = listaCharacteristicsSegV;
    }

    public String getNIIN()
    {
	return NIIN;
    }

    public void setNIIN(String nIIN)
    {
	NIIN = nIIN;
    }

    public String getNSC()
    {
	return nsc;
    }

    public void setNSC(String nSC)
    {
	nsc = nSC;
    }

    public String getRawCharacteristicsGroup()
    {
	return rawCharacteristicsGroup;
    }

    public void setRawCharacteristicsGroup(String rawCharacteristicsGroup)
    {
	this.rawCharacteristicsGroup = rawCharacteristicsGroup;
    }

    public List<String> getListaReplyCode()
    {
	return listaReplyCode;
    }

    /**
     * a lista eh tratada, removendo espacos. Eh vrf ainda se o 1� caracter do MRC
     * nao eh numerico ou se for numerico se eh 9, pois existe MRC comeca com 9000
     * ate 9007 na tabela BR_MRD0107, alem de vrf se o total do comprimento eh > 4,
     * para retirar os MRCs que so vem sem MC e RC.
     * 
     * @author 1T (T) Anders
     * @param listaReplyCode
     * @example de erros encontrados no arquivo: 1500091567110[13F0034#]
     *          1500186995998[2154 CON OPTION-062 (LCD DISPLAY), CAVI E CONNETTORI#]
     */
    public void setListaReplyCode(List<String> listaReplyCode)
    {

	List<String> result = new ArrayList<>();

	for (String code : listaReplyCode) {

	    String v = code.trim();
	    String carMRC = v.substring(0, 1);

	    if ((Helper.isAlphabeticAndLengh4(v.substring(0, 4)) == true & v.length() >= 4)
		    || (v.length() >= 4 & Helper.isNumeric(carMRC) & carMRC.equalsIgnoreCase("9"))) {

		//System.out.println("passou: " + v);
		result.add(v);
	    }// else {
		//System.out.println("nao passou: " + v);
	    //}


	}

	this.listaReplyCode = result;
    }

    public String getInc()
    {
	return inc;
    }

    public void setInc(String inc)
    {
	this.inc = inc;
    }

    public String getInc_raw()
    {
	return inc_raw;
    }

    public void setInc_raw(String inc_raw)
    {
	this.inc_raw = inc_raw;
    }

    public String getInc_nro()
    {
	return inc_nro;
    }

    public void setInc_nro(String inc_nro)
    {
	this.inc_nro = inc_nro;
    }

}
