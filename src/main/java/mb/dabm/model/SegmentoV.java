package mb.dabm.model;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;

public class SegmentoV extends RecursiveTreeObject<SegmentoV>
{
    public ObservableValue<Integer> id;
    public StringProperty NIIN;
    public StringProperty nsc;
    public StringProperty inc;
    public StringProperty mrc;
    public StringProperty mc;
    public StringProperty saic;
    public StringProperty sac;
    public StringProperty rc;
    public StringProperty raw;
    public StringProperty fiig;
    public StringProperty decodeMRC;
    public StringProperty decodeRC;

    public SegmentoV()
    {
	// TODO Auto-generated constructor stub
    }

    SegmentoV(int id, String NIIN, String nsc, String inc, String mrc, String saic, 
	    String sac, String rc, String raw, String mc, String fiig, String decodeMRC, String decodeRc)
    {
	this.id = new SimpleObjectProperty<>(id);
	this.NIIN = new SimpleStringProperty(NIIN);
	this.nsc = new SimpleStringProperty(nsc);
	this.inc = new SimpleStringProperty(inc);
	this.mrc = new SimpleStringProperty(mrc);
	this.saic = new SimpleStringProperty(saic);
	this.sac = new SimpleStringProperty(sac);
	this.rc = new SimpleStringProperty(rc);
	this.raw = new SimpleStringProperty(raw);
	this.mc = new SimpleStringProperty(mc);
	this.fiig = new SimpleStringProperty(fiig);
	this.decodeMRC = new SimpleStringProperty(decodeMRC);
	this.decodeRC = new SimpleStringProperty(decodeRc);
	// this.listaCharacteristicaSegmentoV = new SimpleListProperty<>(lista);
    }
    
    

    public String getFiig()
    {
        return fiig.get();
    }

    public void setFiig(String fiig)
    {
        this.fiig = new SimpleStringProperty(fiig);
    }

    public String getDecodeMRC()
    {
        return decodeMRC.get();
    }

    public void setDecodeMRC(String decodeMRC)
    {
        this.decodeMRC = new SimpleStringProperty(decodeMRC);
    }

    public String getDecodeRC()
    {
        return decodeRC.get();
    }

    public void setDecodeRC(String decodeRC)
    {
        this.decodeRC = new SimpleStringProperty(decodeRC);
    }

    public String getMc()
    {
        return mc.get();
    }

    public void setMc(String mc)
    {
        this.mc = new SimpleStringProperty(mc);
    }

    public String getRaw()
    {
        return raw.get();
    }

    public void setRaw(String raw)
    {
        this.raw = new SimpleStringProperty(raw);
    }

    public void setId(Integer id)
    {
	this.id = new SimpleObjectProperty<>(id);
    }

    public void setNIIN(String niin)
    {

	this.NIIN = new SimpleStringProperty(niin);
    }
    
    public void setNsc(String nsc)
    {
        this.nsc = new SimpleStringProperty(nsc);
    }   
    
    
    public void setInc(String inc)
    {
        this.inc = new SimpleStringProperty(inc);
    }
       

    public void setMrc(String mrc)
    {
        this.mrc = new SimpleStringProperty(mrc);
    }
    
    public void setSaic(String saic)
    {
        this.saic = new SimpleStringProperty(saic);
    }
    
    public void setSac(String sac)
    {
        this.sac = new SimpleStringProperty(sac);
    }
    
    public void setRc(String rc)
    {
        this.rc = new SimpleStringProperty(rc);
    }

    //get
    public Integer getId()
    {
	return id.getValue();
    }

    public final String getNIIN()
    {
	if (NIIN != null) {
	    return NIIN.get();
	}
	return "";
    }

    public String getNsc()
    {
	if (nsc != null) {
	    return nsc.get();
	}
	return "";
    }

    public String getInc()
    {
	if (inc != null) {
	    return inc.get();
	}
	return "";
    }

    public String getSaic()
    {
	if (saic != null) {
	    return saic.get();
	}
	return "";
    }

    public String getMrc()
    {
	if (mrc != null) {
	    return mrc.get();
	}
	return "";
    }

    public String getSac()
    {
	if (sac != null) {
	    return sac.get();
	}
	return "";
    }

    public String getRc()
    {
	if (rc != null) {
	    return rc.get();
	}
	return "";
    }
}
