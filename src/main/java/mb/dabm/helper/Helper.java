package mb.dabm.helper;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.Optional;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

public class Helper
{

    public static boolean isNumeric(String str)
    {
	return str.matches("-?\\d+(\\.\\d+)?");
    }
    
    public static boolean isAlphabeticAndLengh4(String str)
    {
	return str.matches("[A-Z]{4}");
    }

    public static Multimap<String, String> getISAC(String current)
    {
	Multimap<String, String> myMultimap = ArrayListMultimap.create();

	String isac = current.substring(13, 14);
	int tam = Integer.parseInt(isac);
	String sac = current.substring(14, 14 + tam);

	// System.out.println("Linha :" + current);
	// System.out.println("ISAC:" + isac);
	// System.out.println("SAC: " + sac);
	// System.out.println("Tam: "+ tam);
	// System.out.println("Busca: "+ current.indexOf("#"));
	// System.out.println("MC: " + current.substring((14 + tam), (14 + tam) + 1));
	// System.out.println("RC: " + current.substring((14 + tam) + 1,
	// current.indexOf("#")));
	// System.out.println("Restante linha: " +
	// current.substring(current.indexOf("#") + 1));

	myMultimap.put("ISAC", isac);
	myMultimap.put("SAC", sac);
	myMultimap.put("MC", current.substring((14 + tam), (14 + tam) + 1));
	myMultimap.put("RC", current.substring((14 + tam) + 1, current.indexOf("#")));

	return myMultimap;

    }

    /**
     * enviar alerta no sistema
     * 
     * @param type
     * @param titulo
     * @param cabecalho
     * @param conteudo
     */
    public static void enviarAlerta(AlertType type, String titulo, String cabecalho, String conteudo)
    {
	Alert dialog = new Alert(type);
	dialog.setTitle(titulo);
	dialog.setHeaderText(cabecalho);
	dialog.setContentText(conteudo);
	dialog.showAndWait();
    }

    /**
     * http://code.makery.ch/blog/javafx-dialogs-official/ enviar alerta no sistema
     * 
     * @param type
     * @param titulo
     * @param cabecalho
     * @param conteudo
     */
    public static Optional<ButtonType> enviarPergunta(AlertType type, String titulo, String cabecalho, String conteudo)
    {
	Alert dialog = new Alert(type);
	dialog.setTitle(titulo);
	dialog.setHeaderText(cabecalho);
	dialog.setContentText(conteudo);
	// dialog.showAndWait();

	return dialog.showAndWait();
    }

    /**
     * Right way to delete a non empty directory in Java
     * 
     */
    public static boolean deleteDirectory(File dir)
    {
	if (dir.isDirectory()) {
	    File[] children = dir.listFiles();
	    for (int i = 0; i < children.length; i++) {

		boolean success = deleteDirectory(children[i]);

		if (!success) {
		    return false;
		}
	    }
	}
	// either file or an empty directory
	System.out.println("removing file or directory : " + dir.getName());
	return dir.delete();
    }
    
    /**
     * Responsavel por abrir o arquivo especificado
     * 
     * @param file
     */
    public static void openFile(File file)
    {
	Desktop desktop = Desktop.getDesktop();
	
	try {
	    desktop.open(file);
	} catch (IOException ex) {
	    // Logger.getLogger(SegmentVController.class.getName()).log(Level.SEVERE, null,
	    // ex);
	}
    }
}
