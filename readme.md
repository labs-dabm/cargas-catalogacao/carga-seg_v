# Projeto: Catalogação - Carga Segmento V

## Propósito:

O propósito deste trabalho é extrair os dados de características do Item do arquivo RAW DATA, oriundo do CECADE.

## Tecnologia Utilizada no Projeto

Para produção do sistema de **CargaSegV**, estão sendo utilizadas ferramentas reconhecidas no mercado:

* **Java** - Linguagem de programação interpretada orientada a objeto. Diferente das linguagens de programação convencionais, que são compiladas para código nativo, a linguagem Java é compilada para um bytecode que é interpretado por uma máquina virtual (Java Virtual Machine, mais conhecida pela sua abreviação JVM).
* **JavaFX** - Plataforma de software multimídia desenvolvida pela Oracle baseada em **Java** para a criação e disponibilização de Aplicação Rica para Internet que pode ser executada em vários dispositivos diferentes. A versão atual (JavaFX 2.1.0) permite a criação para desktop, browser e dispositivos móveis. TVs, video-games, Blu-rays players e há planos de adicionar novas plataformas no futuro. O suporte nos desktops e browsers é através da JRE e nos dispositivos móveis através do JavaME.
Para construir aplicações os desenvolvedores usam uma linguagem estática tipada e declarada chamada JavaFX Script. No desktop existe implementação para Windows(x86/x64), Mac OS X e Linux (X86/X64). Nos dispositivos móveis, JavaFX é capaz de suportar vários sistemas operativos moveis como Android, Windows Mobile, e outros sistemas proprietários.

* **FXML** - Linguagem de marcação de interface de usuário baseada em XML criada pela Oracle Corporation para definir a interface de usuário de uma aplicação JavaFX . O FXML fornece uma alternativa conveniente para a construção de tais gráficos no código processual e é ideal para definir a interface do usuário de uma aplicação JavaFX , uma vez que a estrutura hierárquica de um documento XML acompanha de perto a estrutura do gráfico de cena JavaFX. No entanto, qualquer coisa que seja criada ou implementada no FXML pode ser expressa usando o JavaFX diretamente. 


## Arquitetura utilizada no Projeto

A figura abaixo, ilustra o fluxo interno da aplicação MVC.

![Arquitetura](wiki/img/FluxoSegV.png)

### Model-View-Control (MVC)

O padrão **model-view-controller (MVC)** permite que você separe o código em diferentes objetos lógicos que servem para tarefas bastante específicas. 
* **Models (Modelos)** - Servem como uma camada de acesso aos dados onde esses dados são requisitados e retornados em formatos nos quais possam ser usados no decorrer de sua aplicação. 
* **Controllers (Controladores)** - Tratam as requisições, processam os dados retornados dos Models e carregam as views (Visões) para enviar a resposta. 
* **Views (Visões)** - São templates de saída (fxml, xml, tpl, html, etc) que são enviadas como resposta.

### Estrutura do Arquivo RAW DATA - Segmento V

![Arquitetura](wiki/img/segV.png)

